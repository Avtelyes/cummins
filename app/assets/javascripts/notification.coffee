ready = ->

  if typeof gon != 'undefined'
    if gon.notification
      $.wdm_notification({
        class: gon.notification.class,
        message: gon.notification.message
        })        

$(document).ready(ready)
$(document).on('page:load', ready)

(->
  $.wdm_notification = (opts) ->
    notification = 
      class: null
      message: null
      init: ->
        _self = this
        navbar = $('body')
        html =    '<div class="general_notification">'
        html +=   '<div class="alert alert-'
        html +=      _self.class
        html +=      ' alert-dismissible" role="alert">'
        html +=      '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
        html +=     _self.message 
        html +=   '</div>'
        html +=   '</div>'
        html = $($.parseHTML(html));
        
        navbar.prepend(html)
        html.delay(15000).fadeOut()

    $.extend(notification, opts)
    notification.init()
) jQuery