ready = ->
  proyect_importance = $('#proyect_importance')
  proyect_importance_text = $('#proyect_importance_text')
  question_container = $('.question_container')
  question = $('#question')

  switch $(proyect_importance).val()
    when "0" 
      question_container.hide()
      $('#proyect_importance_text').prop("required", false)
    when "1" 
      question_container.show(1000)
      $('#proyect_importance_text').prop("required", true)
      question.text("¿Cuál?")
    when "2"
      question_container.show(1000)
      $('#proyect_importance_text').prop("required", true)
      question.text("¿Cómo?")
    when "3"
      question_container.show(1000)
      $('#proyect_importance_text').prop("required", true)
      question.text("¿Cómo?")
    when "4"
      question_container.show(1000)
      $('#proyect_importance_text').prop("required", true)
      question.text("¿Cómo?")
    when "5"
      question_container.show(1000)
      $('#proyect_importance_text').prop("required", true)
      question.text("¿cómo, mediante qué acciones y metodología?")
    when "6"
      question_container.show(1000)
      $('#proyect_importance_text').prop("required", true)
      question.text("¿cómo, mediante qué acciones y metodología?, ¿cuánto, qué cantidad? ¿Quién es mi cliente, cómo tengo seguro que sea mi cliente?")
    when "7"
      question_container.show(1000)
      $('#proyect_importance_text').prop("required", true)
      question.text("¿Cuál?")

  proyect_importance.change ->
    switch $(this).val()
      when "0" 
        question_container.hide(1000)
        $('#proyect_importance_text').prop("required", false)
      when "1" 
        question_container.show(1000)
        $('#proyect_importance_text').prop("required", true)
        question.text("¿Cuál?")
      when "2"
        question_container.show(1000)
        $('#proyect_importance_text').prop("required", true)
        question.text("¿Cómo?")
      when "3"
        question_container.show(1000)
        $('#proyect_importance_text').prop("required", true)
        question.text("¿Cómo?")
      when "4"
        question_container.show(1000)
        $('#proyect_importance_text').prop("required", true)
        question.text("¿Cómo?")
      when "5"
        question_container.show(1000)
        $('#proyect_importance_text').prop("required", true)
        question.text("¿cómo, mediante qué acciones y metodología?")
      when "6"
        question_container.show(1000)
        $('#proyect_importance_text').prop("required", true)
        question.text("¿cómo, mediante qué acciones y metodología?, ¿cuánto, qué cantidad? ¿Quién es mi cliente, cómo tengo seguro que sea mi cliente?")
      when "7"
        question_container.show(1000)
        $('#proyect_importance_text').prop("required", true)
        question.text("¿Cuál?")



$(document).ready(ready)
$(document).on('page:load', ready)