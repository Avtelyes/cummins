ready = ->
  checkboxes = $('input:checkbox')
  text_fields = $('input:text')
  text_fields.hide()

  checkboxes.each (index) ->
    _self = this
    if $(this).prop("checked")
      $(this).next().show()
      $(this).next().prop("required", true)


    $(_self).change -> 
      if $(this).prop("checked")
        $(this).next().show(500)
        $(this).next().prop("required", true)
      else
        $(this).next().hide(500)
        $(this).next().prop("required", false)

$(document).ready(ready)
$(document).on('page:load', ready)