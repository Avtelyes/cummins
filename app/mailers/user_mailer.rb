class UserMailer < ActionMailer::Base

  def account_activation(user)
    mg_client = Mailgun::Client.new "key-79177sakokbq"

    @user = user
    email_with_name = "#{@user.name} <#{@user.email}>"

    message_params = {
      from:     'cummins@testmail.brijex.co',
      to:       email_with_name,
      subject:  'Account Activation.',
      html:     (render_to_string template: 'user_mailer/account_activation' ).to_str
    }

    mg_client.send_message "testmail.brijex.co", message_params
  end
end
