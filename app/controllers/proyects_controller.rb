class ProyectsController < ApplicationController
  before_action :logged_in_user,   except: [:show]
  before_action :correct_user,   except: [:show, :index]
  before_action :editable, except: [:show, :index, :review]
  # before_action :require_admin_session, only: [:index, :show]

  def index
    @proyects = Proyect.all
  end
    
  def show
    @proyect = Proyect.find(params[:id])
  end

  def edit
  end  

  def update
    if @proyect.update_attributes(proyect_params)
      if @proyect.is_new?
        redirect_to :back #justificacion_proyect_path(@proyect)
      else
        redirect_to :back # petition_proyect_path(@proyect)  
      end
    else
      render 'edit'
    end
  end

  def justificacion
  end

  def justificacion_update
    if @proyect.update_attributes(justification_params)
      redirect_to :back #gobjectives_proyect_path(@proyect)
    else
      render 'justificacion'
    end    
  end

  def petition
  end

  def petition_update
    if @proyect.update_attributes(petition_params)
      redirect_to :back #gobjectives_proyect_path(@proyect)
    else
      render 'petition'
    end    
  end

  def gobjectives
  end

  def gobjectives_update
    if @proyect.update_attributes(gobjectives_params)
      redirect_to :back #eobjectives_proyect_path(@proyect)
    else
      render 'gobjectives'
    end
  end

  def eobjectives
  end

  def eobjectives_update
    if @proyect.update_attributes(eobjectives_params)
      redirect_to :back #new_recipient_path # beneficiarios_proyect_path(@proyect)
    else
      render 'gobjectives'
    end
  end

  def beneficiarios_update
    if @proyect.update_attributes(beneficiarios_params)
      redirect_to :back
    else
      redirect_to :back
    end
  end

  def investment
  end

  def investment_update
    if @proyect.update_attributes(investment_params)
      redirect_to :back #continuity_proyect_path(@proyect)
    else
      render 'investment'
    end    
  end

  def continuity
  end

  def continuity_update
    if @proyect.update_attributes(continuity_params)
      redirect_to :back # success_proyect_path(@proyect)
    else
      render 'continuity'
    end
  end

  def success
  end

  def review
  end

  def close_proyect
    @proyect.close
    flash[:success] = "¡Proyecto enviado con éxito!"
    redirect_to :back
    
  end


  private
    def correct_user
      @proyect = Proyect.find(params[:id])
      # user this
      if current_user.proyect != @proyect
        redirect_to edit_proyect_path(current_user.proyect)
      end
    end

    def proyect_params
      params.require(:proyect).permit(:name, :topic, :existent_new)
    end

    def justification_params
      params.require(:proyect).permit(:idea_emerge, :idea_emerge_exp, :procedure_taken, :importance, :importance_text, :importance_text_b,:importance_text_c,:importance_text_d,:importance_text_e,:importance_text_f,:importance_text_g,:importance_text_h)
    end

    def petition_params
      params.require(:proyect).permit(:improvement_importance_a, :improvement_importance_b, :improvement_importance_c, :improvement_importance_d, :improvement_importance_e, :improvement_importance_f)
    end

    def gobjectives_params
      params.require(:proyect).permit(:col_211_a, :col_211_b, :col_211_c, :col_211_d, :col_211_e, :col_212_a, :col_212_b, :col_212_c, :col_212_d, :col_212_e, :col_212_f)
    end

    def eobjectives_params
      params.require(:proyect).permit(:col_oe1, :col_oe1_a, :col_oe1_b, :col_oe1_c, :col_oe1_d, :col_oe1_e, :col_oe2, :col_oe2_a, :col_oe2_b, :col_oe2_c, :col_oe2_d, :col_oe2_e, :col_oe3, :col_oe3_a, :col_oe3_b, :col_oe3_c, :col_oe3_d, :col_oe3_e)
    end

    def beneficiarios_params
      params.require(:proyect).permit(:ben_part_regular, :ben_part_eventual)
    end

    def investment_params
      params.require(:proyect).permit(:investment_a, :investment_b, :investment_c, :investment_d, :investment_e, :investment_f, :investment_sum, :total_weeks)
    end

    def continuity_params
      params.require(:proyect).permit(:col_continuity_action1, :col_continuity_indicator1, :col_continuity_responsible1, :col_continuity_action2, :col_continuity_indicator2, :col_continuity_responsible2, :col_continuity_action3, :col_continuity_indicator3, :col_continuity_responsible3)
    end

    def editable
      unless @proyect.editable?
        flash[:danger] = "El proyecto ha sido enviado. No se puede editar."
        redirect_to review_proyect_path(@proyect)
      end
    end
end