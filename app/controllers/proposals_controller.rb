class ProposalsController < ApplicationController

  def new
    @proposal = Proposal.find_by_user_id(current_user.id)
    if @proposal.nil?
      @proposal = current_user.build_proposal
    end
  end

  def show
    @proposal = Proposal.find(params[:id])
  end

  def update
    @proposal = current_user.proposal.update_attributes(action_params)
    redirect_to :back
  end

  def actions
    @proposal = Proposal.find_by_user_id(current_user.id)
    if @proposal.nil?
      @proposal = current_user.build_proposal
      @proposal.save
    end
  end

  def actions_update
    if @proposal.update_attributes(action_params)
      redirect_to :back #gobjectives_proyect_path(@proyect)
    else
      render 'actions'
    end    
  end

  def activities
  end

  def methodologies
    @proposal = Proposal.find(params[:id])
  end

  def methodologies_update
    @proposal = Proposal.find(params[:id])
    if @proposal.update_attributes(methodologies_params)
      redirect_to :back #gobjectives_proyect_path(@proyect)
    else
      render 'methodologies'
    end
  end

  def impacts
    @proposal = Proposal.find(params[:id])
  end

  def impacts_update
    @proposal = Proposal.find(params[:id])
    if @proposal.update_attributes(impacts_params)
      redirect_to :back #gobjectives_proyect_path(@proyect)
    else
      render 'impacts'
    end
  end

  def resources
  end


  private
  def action_params
      params.require(:proposal).permit(:action_1a, :action_1b, :action_1c, :action_2a, :action_2b, :action_2c, :action_3a, :action_3b, :action_3c)
  end

  def methodologies_params
      params.require(:proposal).permit(:methodology_process, :methodology_medium, :methodology_description)
  end

  def impacts_params
      params.require(:proposal).permit(:impact_objective1, :impact_objective2, :impact_objective3, :impact_goal1, :impact_goal2, :impact_goal3, :impact_indicator1, :impact_indicator2, :impact_indicator3, :impact_evidence1, :impact_evidence2, :impact_evidence3)
  end

end
