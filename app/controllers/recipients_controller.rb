class RecipientsController < ApplicationController
  before_action :logged_in_user

  def new
    @proyect = current_user.proyect
    @recipient = @proyect.recipients.new
  end

  def create
    @recipient = current_user.proyect.recipients.new(recipient_params)
    if @recipient.save
      redirect_to :back
    else
      render 'new'
    end
  end

  def destroy
    current_user.proyect.recipients.find(params[:id]).destroy
    redirect_to new_recipient_path
  end

  private
    def recipient_params
      params.require(:recipient).permit(:ben_group, :ben_age_range, :ben_age_range2, :ben_risk, :ben_risk_exp, :ben_number, :ben_part_regular, :ben_part_eventual)
    end
end