class SectionsController < ApplicationController
  def new
  	@proposal = current_user.proposal
    @section = @proposal.section
  end

  def update
  	@section = current_user.proposal.section.update_attributes(sections_params)
    redirect_to :back
  end

  private
  def sections_params
      params.require(:section).permit(:times_action_1a, :times_action_1b, :times_action_1c, :times_action_2a, 
      		:times_action_2b, :times_action_2c, :times_action_3a, :times_action_3b, :times_action_3c, :section1_fechaI_1a,
      		:section1_fechaI_1b, :section1_fechaI_1c, :section1_fechaI_2a, :section1_fechaI_2b, :section1_fechaI_2c, 
      		:section1_fechaI_3a, :section1_fechaI_3b, :section1_fechaI_3c, :section2_fechaI_1a, :section2_fechaI_1b, 
      		:section2_fechaI_1c, :section2_fechaI_2a, :section2_fechaI_2b, :section2_fechaI_2c, :section2_fechaI_3a, 
      		:section2_fechaI_3b, :section2_fechaI_3c, :section3_fechaI_1a, :section3_fechaI_1b, :section3_fechaI_1c, 
      		:section3_fechaI_2a, :section3_fechaI_2b, :section3_fechaI_2c, :section3_fechaI_3a, :section3_fechaI_3b, 
      		:section3_fechaI_3c, :section4_fechaI_1a, :section4_fechaI_1b, :section4_fechaI_1c, :section4_fechaI_2a, 
      		:section4_fechaI_2b, :section4_fechaI_2c, :section4_fechaI_3a, :section4_fechaI_3b, :section4_fechaI_3c, 
      		:section5_fechaI_1a, :section5_fechaI_1b, :section5_fechaI_1c, :section5_fechaI_2a, :section5_fechaI_2b, 
      		:section5_fechaI_2c, :section5_fechaI_3a, :section5_fechaI_3b, :section5_fechaI_3c, :section6_fechaI_1a, 
      		:section6_fechaI_1b, :section6_fechaI_1c, :section6_fechaI_2a, :section6_fechaI_2b, :section6_fechaI_2c, 
      		:section6_fechaI_3a, :section6_fechaI_3b, :section6_fechaI_3c, :section7_fechaI_1a, :section7_fechaI_1b, 
      		:section7_fechaI_1c, :section7_fechaI_2a, :section7_fechaI_2b, :section7_fechaI_2c, :section7_fechaI_3a, 
      		:section7_fechaI_3b, :section7_fechaI_3c, :section8_fechaI_1a, :section8_fechaI_1b, :section8_fechaI_1c, 
      		:section8_fechaI_2b, :section8_fechaI_2c, :section8_fechaI_3a, :section8_fechaI_3b, :section8_fechaI_3c, 
      		:section9_fechaI_1a, :section9_fechaI_1b, :section9_fechaI_1c, :section9_fechaI_2a, :section9_fechaI_2b, 
      		:section9_fechaI_2c, :section9_fechaI_3a, :section9_fechaI_3b, :section10_fechaI_1a, :section10_fechaI_1b, 
      		:section10_fechaI_1c, :section10_fechaI_2a, :section10_fechaI_2b, :section10_fechaI_2c, :section10_fechaI_3a, 
      		:section10_fechaI_3b, :section10_fechaI_3c, :section11_fechaI_1a, :section11_fechaI_1b, :section11_fechaI_1c,
      		:section11_fechaI_2a, :section11_fechaI_2b, :section11_fechaI_2c, :section11_fechaI_3a, :section11_fechaI_3b, 
      		:section11_fechaI_3c, :section12_fechaI_1a, :section12_fechaI_1b, :section12_fechaI_1c, :section12_fechaI_2a, 
      		:section12_fechaI_2b, :section12_fechaI_2c, :section12_fechaI_3a, :section12_fechaI_3b, :section12_fechaI_3c, 
      		:section1_dias_1a, :section1_dias_1b, :section1_dias_1c, :section1_dias_2a, :section1_dias_2b, 
      		:section1_dias_2c, :section1_dias_3a, :section1_dias_3b, :section1_dias_3c, :section2_dias_1a,
      		:section2_dias_1b, :section2_dias_1c, :section2_dias_2a, :section2_dias_2b, :section2_dias_2c,
      		:section2_dias_3a, :section2_dias_3b, :section2_dias_3c, :section3_dias_1a, :section3_dias_1b,
      		:section3_dias_1c, :section3_dias_2a, :section3_dias_2b, :section3_dias_2c, :section3_dias_3a,
      		:section3_dias_3b, :section3_dias_3c, :section4_dias_1a, :section4_dias_1b, :section4_dias_1c,
      		:section4_dias_2a, :section4_dias_2b, :section4_dias_2c, :section4_dias_3a, :section4_dias_3b,
      		:section4_dias_3c, :section5_dias_1a, :section5_dias_1b, :section5_dias_1c, :section5_dias_2a,
      		:section5_dias_2b, :section5_dias_2c, :section5_dias_3a, :section5_dias_3b, :section5_dias_3c,
      		:section6_dias_1a, :section6_dias_1b, :section6_dias_1c, :section6_dias_2a, :section6_dias_2b,
      		:section6_dias_2c, :section6_dias_3a, :section6_dias_3b, :section6_dias_3c, :section7_dias_1a,
      		:section7_dias_1b, :section7_dias_1c, :section7_dias_2a, :section7_dias_2b, :section7_dias_2c,
      		:section7_dias_3a, :section7_dias_3b, :section7_dias_3c, :section8_dias_1a, :section8_dias_1b,
      		:section8_dias_1c, :section8_dias_2a, :section8_dias_2b, :section8_dias_2c, :section8_dias_3a,
      		:section8_dias_3b, :section8_dias_3c, :section9_dias_1a, :section9_dias_1b, :section9_dias_1c,
      		:section9_dias_2a, :section9_dias_2b, :section9_dias_2c, :section9_dias_3a, :section9_dias_3b,
      		:section9_dias_3c, :section10_dias_1a, :section10_dias_1b, :section10_dias_1c, :section10_dias_2a,
      		:section10_dias_2b, :section10_dias_2c, :section10_dias_3a, :section10_dias_3b, :section10_dias_3c,
      		:section11_dias_1a, :section11_dias_1b, :section11_dias_1c, :section11_dias_2a, :section11_dias_2b,
      		:section11_dias_2c, :section11_dias_3a, :section11_dias_3b, :section11_dias_3c, :section12_dias_1a,
      		:section12_dias_1b, :section12_dias_1c, :section12_dias_2a, :section12_dias_2b, :section12_dias_2c,
      		:section12_dias_3a, :section12_dias_3b, :section12_dias_3c )
  end
end
