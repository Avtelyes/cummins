class UsersController < ApplicationController

  def new
    if logged_in?
      #redirect_to edit_proyect_path(current_user)
      redirect_to acciones_path(current_user)
    else
      @user = User.new
    end
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      #@user.send_activation_email
      flash[:info] = "Un email te fue enviado para activar tu cuenta y completar tu registro"
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  private
    
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation, :phone, :charge, :address, :mission_vision, :yr_operation, :programs_offered, :achievements, :beneficiarios, :comments, :logo, :ac_name)
    end

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      flash[:success] = "Don't even try it ;)"
      redirect_to root_url_for current_user unless current_user?(@user) || current_user.admin?
    end
end
