class SplashController < ApplicationController
  def index
    if flash[:danger]
      gon.notification = {
        class: "danger", 
        message: "Invalid activation link"
      }
    end
      # flash[:info] = "Un email te fue enviado para activar tu cuenta"

    if flash[:info]
      gon.notification = {
        class: "info", 
        message: "Un email te fue enviado para activar tu cuenta y completar tu registro"
      }
    end
  end

  def premio
  end

  def ganadores
  end

  def registro
  end

  def convocatoria
    
  end

  def admin
    @proyects = Proyect.all    
  end
  
end