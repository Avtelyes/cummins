class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include SessionsHelper
  
  def root_url_for(user)
    url = case 
      when user.admin? then admin_cummins_path# admin_root_url
      when user.regular_user? then actions_proposal_path(user) #edit_proyect_path(user.proyect) #splash_index_url #users_root_url
    end
    url
  end

  def require_admin_session
    current_user
    unless @current_user and @current_user.admin?
      flash[:warning] = "Por favor inicia session"
      redirect_to login_path
    end
  end

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Por favor inicia session"
      redirect_to login_path
    end
  end
end