# == Schema Information
#
# Table name: recipients
#
#  id                :integer          not null, primary key
#  proyect_id        :integer          not null
#  ben_part_eventual :integer          default(0)
#  ben_number        :integer
#  ben_age_range     :integer          default(0)
#  ben_age_range2    :integer          default(0)
#  ben_risk          :integer
#  ben_part_regular  :integer          default(0)
#  ben_risk_exp      :string(255)
#  ben_group         :integer
#  updated_at        :datetime         not null
#  created_at        :datetime         not null
#

class Recipient < ActiveRecord::Base
  belongs_to :proyect

  
end
