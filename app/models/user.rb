# == Schema Information
#
# Table name: users
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  ac_name           :string(255)
#  email             :string(255)
#  password_digest   :string(255)
#  remember_digest   :string(255)
#  address           :string(300)
#  phone             :string(15)
#  charge            :string(255)
#  user_type         :integer          default(0), not null
#  activation_digest :string(255)
#  activated         :integer          default(0)
#  activated_at      :datetime
#  reset_digest      :string(255)
#  reset_sent_at     :datetime
#  api_key           :string(255)
#  mission_vision    :string(500)
#  yr_operation      :string(255)
#  achievements      :string(500)
#  programs_offered  :string(255)
#  beneficiarios     :integer
#  comments          :string(500)
#  logo_updated_at   :datetime
#  logo_file_size    :integer
#  logo_content_type :string(255)
#  logo_file_name    :string(255)
#  updated_at        :datetime         not null
#  created_at        :datetime         not null
#

class User < ActiveRecord::Base
  attr_accessor :remember_token, :activation_token, :reset_token

  has_one :proyect
  has_one :proposal

  has_attached_file :logo
  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/
  

  before_create :create_proyect



  before_save   :downcase_email
  before_create :create_activation_digest
  validates :name,  presence: true, length: { maximum: 255 }
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX  },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  
  validates :password, length: { minimum: 6 }, allow_blank: true

  validates :phone, presence: true, length: {maximum: 15}
  validates :mission_vision, length: {maximum: 500}
  validates :achievements, length: {maximum: 500}
  validates :comments, length: {maximum: 500}
  validates :charge, length: {maximum: 255}
  validates :yr_operation, length: {maximum: 255}
  validates :address, presence: true, length: {maximum: 255}


  ACTIVATED = {
    0 => 'no',
    1 => 'yes'
  }

  TYPE_NAME = {
    0 => 'Normal User',
    1 => 'Admin'
  }

  TYPES = {
    :admin    => 1,
    :user     => 0
  }

  def create_proyect
    build_proyect
    true
  end

  def create_proposal
    build_proposal
    true
  end

  before_create do |doc|
    doc.api_key = doc.generate_api_key
  end

  def generate_api_key
    loop do
      token = SecureRandom.base64.tr('+/=', 'Qrt')
      break token unless User.exists?(api_key: token)
    end
  end

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Activates an account.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest, User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  # True if user is admin
  def admin?
    self.user_type == TYPES[:admin]
  end
  
  # True if user is a regular user
  def regular_user?
    self.user_type == TYPES[:user]
  end

  private

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end

end
