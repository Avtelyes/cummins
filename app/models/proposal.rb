class Proposal < ActiveRecord::Base
	belongs_to :user
	has_one :section
	has_one :resource

	before_create :create_section

	def create_section
    	build_section
    	true
  	end
end
