# == Schema Information
#
# Table name: proyects
#
#  id                          :integer          not null, primary key
#  user_id                     :integer          not null
#  logo_updated_at             :datetime
#  logo_content_type           :string(255)
#  logo_file_size              :integer
#  logo_file_name              :string(255)
#  topic                       :integer          default(0)
#  mission_vision              :string(500)
#  yr_operation                :string(255)
#  programs_offered            :string(255)
#  achievements                :string(500)
#  beneficiarios               :integer
#  comments                    :string(500)
#  step                        :integer          default(0)
#  existent_new                :integer          default(0)
#  idea_emerge                 :integer
#  idea_emerge_exp             :string(255)
#  procedure_taken             :integer
#  importance                  :string(255)
#  improvement_importance_a    :string(255)      default("")
#  improvement_importance_b    :string(255)      default("")
#  improvement_importance_c    :string(255)      default("")
#  improvement_importance_d    :string(255)      default("")
#  improvement_importance_e    :string(255)      default("")
#  improvement_importance_f    :string(255)      default("")
#  col_211_a                   :string(255)
#  col_211_b                   :string(255)
#  col_211_c                   :string(255)
#  col_211_d                   :string(255)
#  col_211_e                   :string(255)
#  col_212_a                   :string(255)
#  col_212_b                   :string(255)
#  col_212_c                   :string(255)
#  col_212_d                   :string(255)
#  col_212_e                   :string(255)
#  col_212_f                   :string(255)
#  col_oe1                     :string(255)
#  col_oe1_a                   :string(255)
#  col_oe1_b                   :string(255)
#  col_oe1_c                   :string(255)
#  col_oe1_d                   :string(255)
#  col_oe1_e                   :string(255)
#  col_oe2                     :string(255)
#  col_oe2_a                   :string(255)
#  col_oe2_b                   :string(255)
#  col_oe2_c                   :string(255)
#  col_oe2_d                   :string(255)
#  col_oe2_e                   :string(255)
#  col_oe3                     :string(255)
#  col_oe3_a                   :string(255)
#  col_oe3_b                   :string(255)
#  col_oe3_c                   :string(255)
#  col_oe3_d                   :string(255)
#  col_oe3_e                   :string(255)
#  ben_group                   :integer
#  ben_age_range               :integer          default(0)
#  ben_age_range2              :integer          default(0)
#  ben_risk                    :integer
#  ben_risk_exp                :string(255)
#  ben_number                  :integer          default(1)
#  ben_part_regular            :integer          default(0)
#  ben_part_eventual           :integer          default(0)
#  investment_a                :string(15)
#  investment_b                :string(15)
#  investment_c                :string(15)
#  investment_d                :string(15)
#  investment_e                :string(15)
#  investment_f                :string(255)
#  investment_sum              :string(15)
#  total_weeks                 :integer          default(0)
#  col_continuity_action1      :string(255)
#  col_continuity_indicator1   :string(255)
#  col_continuity_responsible1 :string(255)
#  col_continuity_action2      :string(255)
#  col_continuity_indicator2   :string(255)
#  col_continuity_responsible2 :string(255)
#  col_continuity_action3      :string(255)
#  col_continuity_indicator3   :string(255)
#  col_continuity_responsible3 :string(255)
#  updated_at                  :datetime         not null
#  created_at                  :datetime         not null
#  name                        :string(255)
#  importance_text             :string(255)      default("")
#  benef_regular               :integer
#  benef_eventual              :integer
#  importance_text_b           :string(255)      default("")
#  importance_text_c           :string(255)      default("")
#  importance_text_d           :string(255)      default("")
#  importance_text_e           :string(255)      default("")
#  importance_text_f           :string(255)      default("")
#  importance_text_g           :string(255)      default("")
#  importance_text_h           :string(255)      default("")
#  status                      :integer          default(0)
#

class Proyect < ActiveRecord::Base
  belongs_to :user
  has_many :recipients

  has_attached_file :logo
  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/

  validates :programs_offered, length: {maximum: 255}


  validates :idea_emerge_exp, length: {maximum: 255}
  validates :importance, length: {maximum: 255}
  validates :improvement_importance_a, length: {maximum: 255}
  validates :improvement_importance_b, length: {maximum: 255}
  validates :improvement_importance_c, length: {maximum: 255}
  validates :improvement_importance_d, length: {maximum: 255}
  validates :improvement_importance_e, length: {maximum: 255}
  validates :improvement_importance_f, length: {maximum: 255}
  validates :col_211_a, length: {maximum: 255}
  validates :col_211_b, length: {maximum: 255}
  validates :col_211_c, length: {maximum: 255}
  validates :col_211_d, length: {maximum: 255}
  validates :col_211_e, length: {maximum: 255}
  validates :col_212_a, length: {maximum: 255}
  validates :col_212_b, length: {maximum: 255}
  validates :col_212_c, length: {maximum: 255}
  validates :col_212_d, length: {maximum: 255}
  validates :col_212_e, length: {maximum: 255}
  validates :col_212_f, length: {maximum: 255}
  validates :col_oe1, length: {maximum: 255}
  validates :col_oe1_a, length: {maximum: 255}
  validates :col_oe1_b, length: {maximum: 255}
  validates :col_oe1_c, length: {maximum: 255}
  validates :col_oe1_d, length: {maximum: 255}
  validates :col_oe1_e, length: {maximum: 255}
  validates :col_oe2, length: {maximum: 255}
  validates :col_oe2_a, length: {maximum: 255}
  validates :col_oe2_b, length: {maximum: 255}
  validates :col_oe2_c, length: {maximum: 255}
  validates :col_oe2_d, length: {maximum: 255}
  validates :col_oe2_e, length: {maximum: 255}
  validates :col_oe3, length: {maximum: 255}
  validates :col_oe3_a, length: {maximum: 255}
  validates :col_oe3_b, length: {maximum: 255}
  validates :col_oe3_c, length: {maximum: 255}
  validates :col_oe3_d, length: {maximum: 255}
  validates :col_oe3_e, length: {maximum: 255}
  validates :ben_risk_exp, length: {maximum: 255}
  validates :investment_a, length: {maximum: 15}
  validates :investment_b, length: {maximum: 15}
  validates :investment_c, length: {maximum: 15}
  validates :investment_d, length: {maximum: 15}
  validates :investment_e, length: {maximum: 15}
  validates :investment_f, length: {maximum: 255}
  validates :investment_sum, length: {maximum: 15}
  validates :col_continuity_action1, length: {maximum: 255}
  validates :col_continuity_indicator1, length: {maximum: 255}
  validates :col_continuity_responsible1, length: {maximum: 255}
  validates :col_continuity_action2, length: {maximum: 255}
  validates :col_continuity_indicator2, length: {maximum: 255}
  validates :col_continuity_responsible2, length: {maximum: 255}
  validates :col_continuity_action3, length: {maximum: 255}
  validates :col_continuity_indicator3, length: {maximum: 255}
  validates :col_continuity_responsible3, length: {maximum: 255}
  validates :importance_text_b, length: {maximum: 255}
  validates :importance_text_c, length: {maximum: 255}
  validates :importance_text_d, length: {maximum: 255}
  validates :importance_text_e, length: {maximum: 255}
  validates :importance_text_f, length: {maximum: 255}
  validates :importance_text_g, length: {maximum: 255}
  validates :importance_text_h, length: {maximum: 255}

  SELECT_TOPIC = {
    'Educacion' => 0,
    'Justicia Social' => 1,
    'Medio Ambiente' => 2
  }

  READ_TOPIC = {
    0 => 'Educacion',
    1 => 'Justicia Social',
    2 => 'Medio Ambiente'
  }

  READ_EXISTENT_NEW = {
    0 => 'Petición para mejorar un programa o proyecto ya existente',
    1 => 'Proyecto o programa nuevo'
  }

  READ_IDEA_EMERGE = {
    0 => 'De la comunidad o beneficiarios',
    1 => 'De la OSC',
    2 => 'Otro'
  }

  READ_PROCEDURE_TAKEN = {
    0 => 'Se observó', 
    1 => 'Un estudio', 
    2 => 'Un testimonio'
  }


  SELECT_BENEF_RISK = {
    'Discapacidad'  => 0,
    'Violencia' => 1,
    'Enfermedad' => 2,
    'Abandono' => 3,
    'Marginacion' => 4,
    'Desnutricion' => 5,
    'Otro' => 6
  }

  READ_BENEF_RISK = {
    0 => 'Discapacidad',
    1 => 'Violencia',
    2 => 'Enfermedad',
    3 => 'Abandono',
    4 => 'Marginacion',
    5 => 'Desnutricion',
    6 => 'Otro'
  }

  SELECT_BENEF_GROUP = {
    'Hombres' => 0,
    'Mujeres'  => 1,
    "Ambos"  => 2
  }

  READ_BENEF_GROUP = {
    0 => 'Hombres',
    1 => "Mujeres",
    2 => "Ambos"
  }

  READ_IMPORTANCE = {
    0 => 'Ampliar el número de programas o servicios',
    1 => 'Le doy continuidad a un proyecto anterior',
    2 => 'Mejora la calidad de atención',
    3 => 'Contribuyo a la solución de un problema nuevo en mis beneficiarios',
    4 => 'Mejoro mi operación interna',
    5 => 'Genero un ahorro a mis gastos',
    6 => 'Genero un ingreso nuevo ',
    7 => 'Otro'
  }

  SELECT_IMPORTANCE = {
    'Ampliar el número de programas o servicios' => 0,
    'Le doy continuidad a un proyecto anterior ¿Cuál?' => 1,
    'Mejora la calidad de atención ¿cómo?' => 2,
    'Contribuyo a la solución de un problema nuevo en mis beneficiarios ¿cómo?' => 3,
    'Mejoro mi operación interna ¿cómo?' => 4,
    'Genero un ahorro a mis gastos ¿cómo, mediante qué acciones y metodología?' => 5,
    'Genero un ingreso nuevo ¿cómo, mediante qué acciones y metodología?, ¿cuánto, qué cantidad? ¿Quién es mi cliente, cómo tengo seguro que sea mi cliente?' => 6,
    'Otro ¿Cuál?' => 7
  }

  def is_new?
    self.existent_new == 1
  end

  def beneficiarios
    total = 0
    self.recipients.each do |rec|
      total += rec.ben_number.to_i
    end
    total
  end

  def close
    self.update_attributes(status: 1)
  end

  def editable?
    self.status == 0
  end

end
