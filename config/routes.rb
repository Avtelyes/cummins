Rails.application.routes.draw do
  

  #get 'resource/new'

  #get 'resource/summary'

  get 'sections/new'

  root                           'splash#index'

  post   'login'      =>    'sessions#create'
  delete 'logout'     =>    'sessions#destroy'
  get    'login'      =>    'sessions#new'
  post   'login'      =>    'sessions#create'


  get      'premio'     =>       'splash#premio'
  get      'convocatoria'     =>       'splash#convocatoria'
  get      'ganadores'  =>       'splash#ganadores'
  get      'registro'   =>       'splash#registro' 

  get      'admin-cummins' =>   'proyects#index'

  #get     'acciones'        =>    'proposals#actions'
  #get     'actividades'     =>    'proposals#activities'
  #get     'metodologias'    =>    'proposals#methodologies'
  #get     'impactos'        =>    'proposals#impacts'
  #get     'recursos'        =>    'proposals#resources'

  # get      'justificacion'    =>    'proyects#justificacion'
  # patch    'justificacion'    =>    'proyects#justificacion_update'
  # put      'justificacion'    =>    'proyects#justificacion_update'


  resources :recipients
  resources :sections
  
  resources :account_activations, only: [:edit]
  resources :users#, only: [:new, :create]
  resources :proyects do 
    member do 
      get :justificacion
      patch :justificacion_update

      get :petition
      patch :petition_update

      get :gobjectives
      patch :gobjectives_update

      get :eobjectives
      patch :eobjectives_update

      get :beneficiarios
      patch :beneficiarios_update

      get :investment
      patch :investment_update

      get :continuity
      patch :continuity_update

      get :close_proyect

      get :review

      get :success
    end
  end

  resources :proposals do
    member do
      get :actions
      patch :actions_update

      get :activities
      patch :activities_update

      get :impacts
      patch :impacts_update

      get :methodologies
      patch :methodologies_update

      get :resources
      patch :resources_update
    end
  end

  resources :resource do
    member do

      get :summary
    end
  end

end