# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151006202753) do

  create_table "extras", force: :cascade do |t|
    t.integer  "proyect_id",                 limit: 4,                 null: false
    t.integer  "new_proyect",                limit: 4
    t.integer  "exist_impact_ans_a",         limit: 4,     default: 0
    t.text     "exist_impact_txt_a",         limit: 65535
    t.integer  "exist_impact_ans_b",         limit: 4,     default: 0
    t.text     "exist_impact_txt_b",         limit: 65535
    t.text     "exist_impact_txt_c",         limit: 65535
    t.integer  "exist_impact_ans_d",         limit: 4,     default: 0
    t.text     "exist_impact_txt_d",         limit: 65535
    t.integer  "exist_impact_ans_e",         limit: 4,     default: 0
    t.text     "exist_impact_txt_e",         limit: 65535
    t.integer  "exist_impact_ans_f",         limit: 4,     default: 0
    t.text     "exist_impact_txt_f",         limit: 65535
    t.integer  "exist_impact_ans_g",         limit: 4,     default: 0
    t.text     "exist_impact_txt_g",         limit: 65535
    t.integer  "exist_impact_ans_h",         limit: 4,     default: 0
    t.text     "exist_impact_txt_h",         limit: 65535
    t.integer  "exist_impact_ans_i",         limit: 4,     default: 0
    t.text     "exist_impact_txt_i",         limit: 65535
    t.integer  "exist_impact_ans_j",         limit: 4,     default: 0
    t.text     "exist_impact_txt_j",         limit: 65535
    t.integer  "exist_impact_ans_k",         limit: 4,     default: 0
    t.text     "exist_impact_txt_k",         limit: 65535
    t.integer  "exist_impact_ans_l",         limit: 4,     default: 0
    t.text     "exist_impact_txt_l",         limit: 65535
    t.integer  "exist_impact_ans_m",         limit: 4,     default: 0
    t.text     "exist_impact_txt_m",         limit: 65535
    t.text     "exist_impact_txt_n",         limit: 65535
    t.integer  "exist_impact_ans_o",         limit: 4,     default: 0
    t.text     "exist_impact_txt_o",         limit: 65535
    t.integer  "exist_urgency_ans_a",        limit: 4,     default: 0
    t.text     "exist_urgency_txt_a",        limit: 65535
    t.text     "exist_sustainability_txt_a", limit: 65535
    t.text     "exist_sustainability_txt_b", limit: 65535
    t.integer  "exist_sustainability_ans_c", limit: 4,     default: 0
    t.text     "exist_sustainability_txt_c", limit: 65535
    t.integer  "exist_sustainability_ans_d", limit: 4,     default: 0
    t.text     "exist_sustainability_txt_d", limit: 65535
    t.integer  "exist_scap_ans_a",           limit: 4,     default: 0
    t.text     "exist_scap_txt_a",           limit: 65535
    t.text     "comments",                   limit: 65535
    t.integer  "new_impact_ans_a",           limit: 4,     default: 0
    t.text     "new_impact_txt_a",           limit: 65535
    t.text     "new_impact_txt_b",           limit: 65535
    t.integer  "new_impact_ans_c",           limit: 4,     default: 0
    t.text     "new_impact_txt_c",           limit: 65535
    t.integer  "new_impact_ans_d",           limit: 4,     default: 0
    t.text     "new_impact_txt_d",           limit: 65535
    t.integer  "new_impact_ans_e",           limit: 4,     default: 0
    t.text     "new_impact_txt_e",           limit: 65535
    t.integer  "new_impact_ans_f",           limit: 4,     default: 0
    t.text     "new_impact_txt_f",           limit: 65535
    t.integer  "new_urgency_ans_a",          limit: 4,     default: 0
    t.text     "new_urgency_txt_a",          limit: 65535
    t.integer  "new_urgency_ans_b",          limit: 4,     default: 0
    t.text     "new_urgency_txt_b",          limit: 65535
    t.text     "new_sustainability_txt_a",   limit: 65535
    t.text     "new_sustainability_txt_b",   limit: 65535
    t.integer  "new_sustainability_ans_c",   limit: 4,     default: 0
    t.text     "new_sustainability_txt_c",   limit: 65535
    t.integer  "new_sustainability_ans_d",   limit: 4,     default: 0
    t.text     "new_sustainability_txt_d",   limit: 65535
    t.integer  "new_sustainability_ans_e",   limit: 4,     default: 0
    t.text     "new_sustainability_txt_e",   limit: 65535
    t.integer  "new_scap_ans_a",             limit: 4,     default: 0
    t.text     "new_scap_txt_a",             limit: 65535
    t.integer  "new_scap_ans_b",             limit: 4,     default: 0
    t.text     "new_scap_txt_b",             limit: 65535
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
  end

  add_index "extras", ["proyect_id"], name: "fk_rails_661195d9a6", using: :btree

  create_table "proposals", force: :cascade do |t|
    t.string   "action_1a",               limit: 255
    t.string   "action_1b",               limit: 255
    t.string   "action_1c",               limit: 255
    t.string   "action_2a",               limit: 255
    t.string   "action_2b",               limit: 255
    t.string   "action_2c",               limit: 255
    t.string   "action_3a",               limit: 255
    t.string   "action_3b",               limit: 255
    t.string   "action_3c",               limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "user_id",                 limit: 4
    t.string   "methodology_process",     limit: 255
    t.string   "methodology_medium",      limit: 255
    t.string   "methodology_description", limit: 255
    t.string   "impact_objective1",       limit: 255
    t.string   "impact_objective2",       limit: 255
    t.string   "impact_objective3",       limit: 255
    t.string   "impact_goal1",            limit: 255
    t.string   "impact_goal2",            limit: 255
    t.string   "impact_goal3",            limit: 255
    t.string   "impact_indicator1",       limit: 255
    t.string   "impact_indicator2",       limit: 255
    t.string   "impact_indicator3",       limit: 255
    t.string   "impact_evidence1",        limit: 255
    t.string   "impact_evidence2",        limit: 255
    t.string   "impact_evidence3",        limit: 255
  end

  add_index "proposals", ["user_id"], name: "index_proposals_on_user_id", using: :btree

  create_table "proyects", force: :cascade do |t|
    t.integer  "user_id",                     limit: 4,                  null: false
    t.datetime "logo_updated_at"
    t.string   "logo_content_type",           limit: 255
    t.integer  "logo_file_size",              limit: 4
    t.string   "logo_file_name",              limit: 255
    t.integer  "topic",                       limit: 1,     default: 0
    t.string   "mission_vision",              limit: 500
    t.string   "yr_operation",                limit: 255
    t.string   "programs_offered",            limit: 255
    t.string   "achievements",                limit: 500
    t.integer  "beneficiarios",               limit: 4
    t.string   "comments",                    limit: 500
    t.integer  "step",                        limit: 1,     default: 0
    t.integer  "existent_new",                limit: 4,     default: 0
    t.integer  "idea_emerge",                 limit: 4
    t.string   "idea_emerge_exp",             limit: 255
    t.integer  "procedure_taken",             limit: 4
    t.string   "importance",                  limit: 255
    t.string   "improvement_importance_a",    limit: 255,   default: ""
    t.string   "improvement_importance_b",    limit: 255,   default: ""
    t.string   "improvement_importance_c",    limit: 255,   default: ""
    t.string   "improvement_importance_d",    limit: 255,   default: ""
    t.string   "improvement_importance_e",    limit: 255,   default: ""
    t.string   "improvement_importance_f",    limit: 255,   default: ""
    t.string   "col_211_a",                   limit: 255
    t.string   "col_211_b",                   limit: 255
    t.string   "col_211_c",                   limit: 255
    t.string   "col_211_d",                   limit: 255
    t.string   "col_211_e",                   limit: 255
    t.string   "col_212_a",                   limit: 255
    t.string   "col_212_b",                   limit: 255
    t.string   "col_212_c",                   limit: 255
    t.string   "col_212_d",                   limit: 255
    t.string   "col_212_e",                   limit: 255
    t.string   "col_212_f",                   limit: 255
    t.string   "col_oe1",                     limit: 255
    t.string   "col_oe1_a",                   limit: 255
    t.string   "col_oe1_b",                   limit: 255
    t.string   "col_oe1_c",                   limit: 255
    t.string   "col_oe1_d",                   limit: 255
    t.string   "col_oe1_e",                   limit: 255
    t.string   "col_oe2",                     limit: 255
    t.string   "col_oe2_a",                   limit: 255
    t.string   "col_oe2_b",                   limit: 255
    t.string   "col_oe2_c",                   limit: 255
    t.string   "col_oe2_d",                   limit: 255
    t.string   "col_oe2_e",                   limit: 255
    t.string   "col_oe3",                     limit: 255
    t.string   "col_oe3_a",                   limit: 255
    t.string   "col_oe3_b",                   limit: 255
    t.string   "col_oe3_c",                   limit: 255
    t.string   "col_oe3_d",                   limit: 255
    t.string   "col_oe3_e",                   limit: 255
    t.integer  "ben_group",                   limit: 1
    t.integer  "ben_age_range",               limit: 4,     default: 0
    t.integer  "ben_age_range2",              limit: 4,     default: 0
    t.integer  "ben_risk",                    limit: 4
    t.string   "ben_risk_exp",                limit: 255
    t.integer  "ben_number",                  limit: 4,     default: 1
    t.integer  "ben_part_regular",            limit: 4,     default: 0
    t.integer  "ben_part_eventual",           limit: 4,     default: 0
    t.string   "investment_a",                limit: 15
    t.string   "investment_b",                limit: 15
    t.string   "investment_c",                limit: 15
    t.string   "investment_d",                limit: 15
    t.string   "investment_e",                limit: 15
    t.string   "investment_f",                limit: 255
    t.string   "investment_sum",              limit: 15
    t.integer  "total_weeks",                 limit: 4,     default: 0
    t.string   "col_continuity_action1",      limit: 255
    t.string   "col_continuity_indicator1",   limit: 255
    t.string   "col_continuity_responsible1", limit: 255
    t.string   "col_continuity_action2",      limit: 255
    t.string   "col_continuity_indicator2",   limit: 255
    t.string   "col_continuity_responsible2", limit: 255
    t.string   "col_continuity_action3",      limit: 255
    t.string   "col_continuity_indicator3",   limit: 255
    t.string   "col_continuity_responsible3", limit: 255
    t.datetime "updated_at",                                             null: false
    t.datetime "created_at",                                             null: false
    t.string   "name",                        limit: 255
    t.text     "importance_text",             limit: 65535
    t.integer  "benef_regular",               limit: 4
    t.integer  "benef_eventual",              limit: 4
    t.string   "importance_text_b",           limit: 255
    t.string   "importance_text_c",           limit: 255
    t.string   "importance_text_d",           limit: 255
    t.string   "importance_text_e",           limit: 255
    t.string   "importance_text_f",           limit: 255
    t.string   "importance_text_g",           limit: 255
    t.string   "importance_text_h",           limit: 255
    t.integer  "status",                      limit: 4,     default: 0
  end

  add_index "proyects", ["user_id"], name: "users_proyects_fk", using: :btree

  create_table "recipients", force: :cascade do |t|
    t.integer  "proyect_id",        limit: 4,               null: false
    t.integer  "ben_part_eventual", limit: 4,   default: 0
    t.integer  "ben_number",        limit: 4
    t.integer  "ben_age_range",     limit: 4,   default: 0
    t.integer  "ben_age_range2",    limit: 4,   default: 0
    t.integer  "ben_risk",          limit: 4
    t.integer  "ben_part_regular",  limit: 4,   default: 0
    t.string   "ben_risk_exp",      limit: 255
    t.integer  "ben_group",         limit: 1
    t.datetime "updated_at",                                null: false
    t.datetime "created_at",                                null: false
  end

  add_index "recipients", ["proyect_id"], name: "proyects_recipients_fk", using: :btree

  create_table "resources", force: :cascade do |t|
    t.string   "concept1_action_1a",        limit: 30
    t.string   "concept2_action_1a",        limit: 30
    t.string   "concept3_action_1a",        limit: 30
    t.string   "concept4_action_1a",        limit: 30
    t.string   "concept5_action_1a",        limit: 30
    t.string   "concept1_action_1b",        limit: 30
    t.string   "concept2_action_1b",        limit: 30
    t.string   "concept3_action_1b",        limit: 30
    t.string   "concept4_action_1b",        limit: 30
    t.string   "concept5_action_1b",        limit: 30
    t.string   "concept1_action_1c",        limit: 30
    t.string   "concept2_action_1c",        limit: 30
    t.string   "concept3_action_1c",        limit: 30
    t.string   "concept4_action_1c",        limit: 30
    t.string   "concept5_action_1c",        limit: 30
    t.string   "concept1_action_2a",        limit: 30
    t.string   "concept2_action_2a",        limit: 30
    t.string   "concept3_action_2a",        limit: 30
    t.string   "concept4_action_2a",        limit: 30
    t.string   "concept5_action_2a",        limit: 30
    t.string   "concept1_action_2b",        limit: 30
    t.string   "concept2_action_2b",        limit: 30
    t.string   "concept3_action_2b",        limit: 30
    t.string   "concept4_action_2b",        limit: 30
    t.string   "concept5_action_2b",        limit: 30
    t.string   "concept1_action_2c",        limit: 30
    t.string   "concept2_action_2c",        limit: 30
    t.string   "concept3_action_2c",        limit: 30
    t.string   "concept4_action_2c",        limit: 30
    t.string   "concept5_action_2c",        limit: 30
    t.string   "concept1_action_3a",        limit: 30
    t.string   "concept2_action_3a",        limit: 30
    t.string   "concept3_action_3a",        limit: 30
    t.string   "concept4_action_3a",        limit: 30
    t.string   "concept5_action_3a",        limit: 30
    t.string   "concept1_action_3b",        limit: 30
    t.string   "concept2_action_3b",        limit: 30
    t.string   "concept3_action_3b",        limit: 30
    t.string   "concept4_action_3b",        limit: 30
    t.string   "concept5_action_3b",        limit: 30
    t.string   "concept1_action_3c",        limit: 30
    t.string   "concept2_action_3c",        limit: 30
    t.string   "concept3_action_3c",        limit: 30
    t.string   "concept4_action_3c",        limit: 30
    t.string   "concept5_action_3c",        limit: 30
    t.string   "clasif1_action_1a",         limit: 30
    t.string   "clasif2_action_1a",         limit: 30
    t.string   "clasif3_action_1a",         limit: 30
    t.string   "clasif4_action_1a",         limit: 30
    t.string   "clasif5_action_1a",         limit: 30
    t.string   "clasif1_action_1b",         limit: 30
    t.string   "clasif2_action_1b",         limit: 30
    t.string   "clasif3_action_1b",         limit: 30
    t.string   "clasif4_action_1b",         limit: 30
    t.string   "clasif5_action_1b",         limit: 30
    t.string   "clasif1_action_1c",         limit: 30
    t.string   "clasif2_action_1c",         limit: 30
    t.string   "clasif3_action_1c",         limit: 30
    t.string   "clasif4_action_1c",         limit: 30
    t.string   "clasif5_action_1c",         limit: 30
    t.string   "clasif1_action_2a",         limit: 30
    t.string   "clasif2_action_2a",         limit: 30
    t.string   "clasif3_action_2a",         limit: 30
    t.string   "clasif4_action_2a",         limit: 30
    t.string   "clasif5_action_2a",         limit: 30
    t.string   "clasif1_action_2b",         limit: 30
    t.string   "clasif2_action_2b",         limit: 30
    t.string   "clasif3_action_2b",         limit: 30
    t.string   "clasif4_action_2b",         limit: 30
    t.string   "clasif5_action_2b",         limit: 30
    t.string   "clasif1_action_2c",         limit: 30
    t.string   "clasif2_action_2c",         limit: 30
    t.string   "clasif3_action_2c",         limit: 30
    t.string   "clasif4_action_2c",         limit: 30
    t.string   "clasif5_action_2c",         limit: 30
    t.string   "clasif1_action_3a",         limit: 30
    t.string   "clasif2_action_3a",         limit: 30
    t.string   "clasif3_action_3a",         limit: 30
    t.string   "clasif4_action_3a",         limit: 30
    t.string   "clasif5_action_3a",         limit: 30
    t.string   "clasif1_action_3b",         limit: 30
    t.string   "clasif2_action_3b",         limit: 30
    t.string   "clasif3_action_3b",         limit: 30
    t.string   "clasif4_action_3b",         limit: 30
    t.string   "clasif5_action_3b",         limit: 30
    t.string   "clasif1_action_3c",         limit: 30
    t.string   "clasif2_action_3c",         limit: 30
    t.string   "clasif3_action_3c",         limit: 30
    t.string   "clasif4_action_3c",         limit: 30
    t.string   "clasif5_action_3c",         limit: 30
    t.string   "name1_prov1_action_1a",     limit: 30
    t.string   "name1_prov2_action_1a",     limit: 30
    t.string   "name1_prov3_action_1a",     limit: 30
    t.string   "name1_prov4_action_1a",     limit: 30
    t.string   "name1_prov5_action_1a",     limit: 30
    t.string   "name1_prov1_action_1b",     limit: 30
    t.string   "name1_prov2_action_1b",     limit: 30
    t.string   "name1_prov3_action_1b",     limit: 30
    t.string   "name1_prov4_action_1b",     limit: 30
    t.string   "name1_prov5_action_1b",     limit: 30
    t.string   "name1_prov1_action_1c",     limit: 30
    t.string   "name1_prov2_action_1c",     limit: 30
    t.string   "name1_prov3_action_1c",     limit: 30
    t.string   "name1_prov4_action_1c",     limit: 30
    t.string   "name1_prov5_action_1c",     limit: 30
    t.string   "name1_prov1_action_2a",     limit: 30
    t.string   "name1_prov2_action_2a",     limit: 30
    t.string   "name1_prov3_action_2a",     limit: 30
    t.string   "name1_prov4_action_2a",     limit: 30
    t.string   "name1_prov5_action_2a",     limit: 30
    t.string   "name1_prov1_action_2b",     limit: 30
    t.string   "name1_prov2_action_2b",     limit: 30
    t.string   "name1_prov3_action_2b",     limit: 30
    t.string   "name1_prov4_action_2b",     limit: 30
    t.string   "name1_prov5_action_2b",     limit: 30
    t.string   "name1_prov1_action_2c",     limit: 30
    t.string   "name1_prov2_action_2c",     limit: 30
    t.string   "name1_prov3_action_2c",     limit: 30
    t.string   "name1_prov4_action_2c",     limit: 30
    t.string   "name1_prov5_action_2c",     limit: 30
    t.string   "name1_prov1_action_3a",     limit: 30
    t.string   "name1_prov2_action_3a",     limit: 30
    t.string   "name1_prov3_action_3a",     limit: 30
    t.string   "name1_prov4_action_3a",     limit: 30
    t.string   "name1_prov5_action_3a",     limit: 30
    t.string   "name1_prov1_action_3b",     limit: 30
    t.string   "name1_prov2_action_3b",     limit: 30
    t.string   "name1_prov3_action_3b",     limit: 30
    t.string   "name1_prov4_action_3b",     limit: 30
    t.string   "name1_prov5_action_3b",     limit: 30
    t.string   "name1_prov1_action_3c",     limit: 30
    t.string   "name1_prov2_action_3c",     limit: 30
    t.string   "name1_prov3_action_3c",     limit: 30
    t.string   "name1_prov4_action_3c",     limit: 30
    t.string   "name1_prov5_action_3c",     limit: 30
    t.integer  "cost1_action1_1a",          limit: 4
    t.integer  "cost1_action2_1a",          limit: 4
    t.integer  "cost1_action3_1a",          limit: 4
    t.integer  "cost1_action4_1a",          limit: 4
    t.integer  "cost1_action5_1a",          limit: 4
    t.integer  "cost1_action1_1b",          limit: 4
    t.integer  "cost1_action2_1b",          limit: 4
    t.integer  "cost1_action3_1b",          limit: 4
    t.integer  "cost1_action4_1b",          limit: 4
    t.integer  "cost1_action5_1b",          limit: 4
    t.integer  "cost1_action1_1c",          limit: 4
    t.integer  "cost1_action2_1c",          limit: 4
    t.integer  "cost1_action3_1c",          limit: 4
    t.integer  "cost1_action4_1c",          limit: 4
    t.integer  "cost1_action5_1c",          limit: 4
    t.integer  "cost1_action1_2a",          limit: 4
    t.integer  "cost1_action2_2a",          limit: 4
    t.integer  "cost1_action3_2a",          limit: 4
    t.integer  "cost1_action4_2a",          limit: 4
    t.integer  "cost1_action5_2a",          limit: 4
    t.integer  "cost1_action1_2b",          limit: 4
    t.integer  "cost1_action2_2b",          limit: 4
    t.integer  "cost1_action3_2b",          limit: 4
    t.integer  "cost1_action4_2b",          limit: 4
    t.integer  "cost1_action5_2b",          limit: 4
    t.integer  "cost1_action1_2c",          limit: 4
    t.integer  "cost1_action2_2c",          limit: 4
    t.integer  "cost1_action3_2c",          limit: 4
    t.integer  "cost1_action4_2c",          limit: 4
    t.integer  "cost1_action5_2c",          limit: 4
    t.integer  "cost1_action1_3a",          limit: 4
    t.integer  "cost1_action2_3a",          limit: 4
    t.integer  "cost1_action3_3a",          limit: 4
    t.integer  "cost1_action4_3a",          limit: 4
    t.integer  "cost1_action5_3a",          limit: 4
    t.integer  "cost1_action1_3b",          limit: 4
    t.integer  "cost1_action2_3b",          limit: 4
    t.integer  "cost1_action3_3b",          limit: 4
    t.integer  "cost1_action4_3b",          limit: 4
    t.integer  "cost1_action5_3b",          limit: 4
    t.integer  "cost1_action1_3c",          limit: 4
    t.integer  "cost1_action2_3c",          limit: 4
    t.integer  "cost1_action3_3c",          limit: 4
    t.integer  "cost1_action4_3c",          limit: 4
    t.integer  "cost1_action5_3c",          limit: 4
    t.string   "name2_prov1_action_1a",     limit: 30
    t.string   "name2_prov2_action_1a",     limit: 30
    t.string   "name2_prov3_action_1a",     limit: 30
    t.string   "name2_prov4_action_1a",     limit: 30
    t.string   "name2_prov5_action_1a",     limit: 30
    t.string   "name2_prov1_action_1b",     limit: 30
    t.string   "name2_prov2_action_1b",     limit: 30
    t.string   "name2_prov3_action_1b",     limit: 30
    t.string   "name2_prov4_action_1b",     limit: 30
    t.string   "name2_prov5_action_1b",     limit: 30
    t.string   "name2_prov1_action_1c",     limit: 30
    t.string   "name2_prov2_action_1c",     limit: 30
    t.string   "name2_prov3_action_1c",     limit: 30
    t.string   "name2_prov4_action_1c",     limit: 30
    t.string   "name2_prov5_action_1c",     limit: 30
    t.string   "name2_prov1_action_2a",     limit: 30
    t.string   "name2_prov2_action_2a",     limit: 30
    t.string   "name2_prov3_action_2a",     limit: 30
    t.string   "name2_prov4_action_2a",     limit: 30
    t.string   "name2_prov5_action_2a",     limit: 30
    t.string   "name2_prov1_action_2b",     limit: 30
    t.string   "name2_prov2_action_2b",     limit: 30
    t.string   "name2_prov3_action_2b",     limit: 30
    t.string   "name2_prov4_action_2b",     limit: 30
    t.string   "name2_prov5_action_2b",     limit: 30
    t.string   "name2_prov1_action_2c",     limit: 30
    t.string   "name2_prov2_action_2c",     limit: 30
    t.string   "name2_prov3_action_2c",     limit: 30
    t.string   "name2_prov4_action_2c",     limit: 30
    t.string   "name2_prov5_action_2c",     limit: 30
    t.string   "name2_prov1_action_3a",     limit: 30
    t.string   "name2_prov2_action_3a",     limit: 30
    t.string   "name2_prov3_action_3a",     limit: 30
    t.string   "name2_prov4_action_3a",     limit: 30
    t.string   "name2_prov5_action_3a",     limit: 30
    t.string   "name2_prov1_action_3b",     limit: 30
    t.string   "name2_prov2_action_3b",     limit: 30
    t.string   "name2_prov3_action_3b",     limit: 30
    t.string   "name2_prov4_action_3b",     limit: 30
    t.string   "name2_prov5_action_3b",     limit: 30
    t.string   "name2_prov1_action_3c",     limit: 30
    t.string   "name2_prov2_action_3c",     limit: 30
    t.string   "name2_prov3_action_3c",     limit: 30
    t.string   "name2_prov4_action_3c",     limit: 30
    t.string   "name2_prov5_action_3c",     limit: 30
    t.integer  "cost2_action1_1a",          limit: 4
    t.integer  "cost2_action2_1a",          limit: 4
    t.integer  "cost2_action3_1a",          limit: 4
    t.integer  "cost2_action4_1a",          limit: 4
    t.integer  "cost2_action5_1a",          limit: 4
    t.integer  "cost2_action1_1b",          limit: 4
    t.integer  "cost2_action2_1b",          limit: 4
    t.integer  "cost2_action3_1b",          limit: 4
    t.integer  "cost2_action4_1b",          limit: 4
    t.integer  "cost2_action5_1b",          limit: 4
    t.integer  "cost2_action1_1c",          limit: 4
    t.integer  "cost2_action2_1c",          limit: 4
    t.integer  "cost2_action3_1c",          limit: 4
    t.integer  "cost2_action4_1c",          limit: 4
    t.integer  "cost2_action5_1c",          limit: 4
    t.integer  "cost2_action1_2a",          limit: 4
    t.integer  "cost2_action2_2a",          limit: 4
    t.integer  "cost2_action3_2a",          limit: 4
    t.integer  "cost2_action4_2a",          limit: 4
    t.integer  "cost2_action5_2a",          limit: 4
    t.integer  "cost2_action1_2b",          limit: 4
    t.integer  "cost2_action2_2b",          limit: 4
    t.integer  "cost2_action3_2b",          limit: 4
    t.integer  "cost2_action4_2b",          limit: 4
    t.integer  "cost2_action5_2b",          limit: 4
    t.integer  "cost2_action1_2c",          limit: 4
    t.integer  "cost2_action2_2c",          limit: 4
    t.integer  "cost2_action3_2c",          limit: 4
    t.integer  "cost2_action4_2c",          limit: 4
    t.integer  "cost2_action5_2c",          limit: 4
    t.integer  "cost2_action1_3a",          limit: 4
    t.integer  "cost2_action2_3a",          limit: 4
    t.integer  "cost2_action3_3a",          limit: 4
    t.integer  "cost2_action4_3a",          limit: 4
    t.integer  "cost2_action5_3a",          limit: 4
    t.integer  "cost2_action1_3b",          limit: 4
    t.integer  "cost2_action2_3b",          limit: 4
    t.integer  "cost2_action3_3b",          limit: 4
    t.integer  "cost2_action4_3b",          limit: 4
    t.integer  "cost2_action5_3b",          limit: 4
    t.integer  "cost2_action1_3c",          limit: 4
    t.integer  "cost2_action2_3c",          limit: 4
    t.integer  "cost2_action3_3c",          limit: 4
    t.integer  "cost2_action4_3c",          limit: 4
    t.integer  "cost2_action5_3c",          limit: 4
    t.string   "name3_prov1_action_1a",     limit: 30
    t.string   "name3_prov2_action_1a",     limit: 30
    t.string   "name3_prov3_action_1a",     limit: 30
    t.string   "name3_prov4_action_1a",     limit: 30
    t.string   "name3_prov5_action_1a",     limit: 30
    t.string   "name3_prov1_action_1b",     limit: 30
    t.string   "name3_prov2_action_1b",     limit: 30
    t.string   "name3_prov3_action_1b",     limit: 30
    t.string   "name3_prov4_action_1b",     limit: 30
    t.string   "name3_prov5_action_1b",     limit: 30
    t.string   "name3_prov1_action_1c",     limit: 30
    t.string   "name3_prov2_action_1c",     limit: 30
    t.string   "name3_prov3_action_1c",     limit: 30
    t.string   "name3_prov4_action_1c",     limit: 30
    t.string   "name3_prov5_action_1c",     limit: 30
    t.string   "name3_prov1_action_2a",     limit: 30
    t.string   "name3_prov2_action_2a",     limit: 30
    t.string   "name3_prov3_action_2a",     limit: 30
    t.string   "name3_prov4_action_2a",     limit: 30
    t.string   "name3_prov5_action_2a",     limit: 30
    t.string   "name3_prov1_action_2b",     limit: 30
    t.string   "name3_prov2_action_2b",     limit: 30
    t.string   "name3_prov3_action_2b",     limit: 30
    t.string   "name3_prov4_action_2b",     limit: 30
    t.string   "name3_prov5_action_2b",     limit: 30
    t.string   "name3_prov1_action_2c",     limit: 30
    t.string   "name3_prov2_action_2c",     limit: 30
    t.string   "name3_prov3_action_2c",     limit: 30
    t.string   "name3_prov4_action_2c",     limit: 30
    t.string   "name3_prov5_action_2c",     limit: 30
    t.string   "name3_prov1_action_3a",     limit: 30
    t.string   "name3_prov2_action_3a",     limit: 30
    t.string   "name3_prov3_action_3a",     limit: 30
    t.string   "name3_prov4_action_3a",     limit: 30
    t.string   "name3_prov5_action_3a",     limit: 30
    t.string   "name3_prov1_action_3b",     limit: 30
    t.string   "name3_prov2_action_3b",     limit: 30
    t.string   "name3_prov3_action_3b",     limit: 30
    t.string   "name3_prov4_action_3b",     limit: 30
    t.string   "name3_prov5_action_3b",     limit: 30
    t.string   "name3_prov1_action_3c",     limit: 30
    t.string   "name3_prov2_action_3c",     limit: 30
    t.string   "name3_prov3_action_3c",     limit: 30
    t.string   "name3_prov4_action_3c",     limit: 30
    t.string   "name3_prov5_action_3c",     limit: 30
    t.integer  "cost3_action1_1a",          limit: 4
    t.integer  "cost3_action2_1a",          limit: 4
    t.integer  "cost3_action3_1a",          limit: 4
    t.integer  "cost3_action4_1a",          limit: 4
    t.integer  "cost3_action5_1a",          limit: 4
    t.integer  "cost3_action1_1b",          limit: 4
    t.integer  "cost3_action2_1b",          limit: 4
    t.integer  "cost3_action3_1b",          limit: 4
    t.integer  "cost3_action4_1b",          limit: 4
    t.integer  "cost3_action5_1b",          limit: 4
    t.integer  "cost3_action1_1c",          limit: 4
    t.integer  "cost3_action2_1c",          limit: 4
    t.integer  "cost3_action3_1c",          limit: 4
    t.integer  "cost3_action4_1c",          limit: 4
    t.integer  "cost3_action5_1c",          limit: 4
    t.integer  "cost3_action1_2a",          limit: 4
    t.integer  "cost3_action2_2a",          limit: 4
    t.integer  "cost3_action3_2a",          limit: 4
    t.integer  "cost3_action4_2a",          limit: 4
    t.integer  "cost3_action5_2a",          limit: 4
    t.integer  "cost3_action1_2b",          limit: 4
    t.integer  "cost3_action2_2b",          limit: 4
    t.integer  "cost3_action3_2b",          limit: 4
    t.integer  "cost3_action4_2b",          limit: 4
    t.integer  "cost3_action5_2b",          limit: 4
    t.integer  "cost3_action1_2c",          limit: 4
    t.integer  "cost3_action2_2c",          limit: 4
    t.integer  "cost3_action3_2c",          limit: 4
    t.integer  "cost3_action4_2c",          limit: 4
    t.integer  "cost3_action5_2c",          limit: 4
    t.integer  "cost3_action1_3a",          limit: 4
    t.integer  "cost3_action2_3a",          limit: 4
    t.integer  "cost3_action3_3a",          limit: 4
    t.integer  "cost3_action4_3a",          limit: 4
    t.integer  "cost3_action5_3a",          limit: 4
    t.integer  "cost3_action1_3b",          limit: 4
    t.integer  "cost3_action2_3b",          limit: 4
    t.integer  "cost3_action3_3b",          limit: 4
    t.integer  "cost3_action4_3b",          limit: 4
    t.integer  "cost3_action5_3b",          limit: 4
    t.integer  "cost3_action1_3c",          limit: 4
    t.integer  "cost3_action2_3c",          limit: 4
    t.integer  "cost3_action3_3c",          limit: 4
    t.integer  "cost3_action4_3c",          limit: 4
    t.integer  "cost3_action5_3c",          limit: 4
    t.string   "selection_prov1_action_1a", limit: 30
    t.string   "selection_prov2_action_1a", limit: 30
    t.string   "selection_prov3_action_1a", limit: 30
    t.string   "selection_prov4_action_1a", limit: 30
    t.string   "selection_prov5_action_1a", limit: 30
    t.string   "selection_prov1_action_1b", limit: 30
    t.string   "selection_prov2_action_1b", limit: 30
    t.string   "selection_prov3_action_1b", limit: 30
    t.string   "selection_prov4_action_1b", limit: 30
    t.string   "selection_prov5_action_1b", limit: 30
    t.string   "selection_prov1_action_1c", limit: 30
    t.string   "selection_prov2_action_1c", limit: 30
    t.string   "selection_prov3_action_1c", limit: 30
    t.string   "selection_prov4_action_1c", limit: 30
    t.string   "selection_prov5_action_1c", limit: 30
    t.string   "selection_prov1_action_2a", limit: 30
    t.string   "selection_prov2_action_2a", limit: 30
    t.string   "selection_prov3_action_2a", limit: 30
    t.string   "selection_prov4_action_2a", limit: 30
    t.string   "selection_prov5_action_2a", limit: 30
    t.string   "selection_prov1_action_2b", limit: 30
    t.string   "selection_prov2_action_2b", limit: 30
    t.string   "selection_prov3_action_2b", limit: 30
    t.string   "selection_prov4_action_2b", limit: 30
    t.string   "selection_prov5_action_2b", limit: 30
    t.string   "selection_prov1_action_2c", limit: 30
    t.string   "selection_prov2_action_2c", limit: 30
    t.string   "selection_prov3_action_2c", limit: 30
    t.string   "selection_prov4_action_2c", limit: 30
    t.string   "selection_prov5_action_2c", limit: 30
    t.string   "selection_prov1_action_3a", limit: 30
    t.string   "selection_prov2_action_3a", limit: 30
    t.string   "selection_prov3_action_3a", limit: 30
    t.string   "selection_prov4_action_3a", limit: 30
    t.string   "selection_prov5_action_3a", limit: 30
    t.string   "selection_prov1_action_3b", limit: 30
    t.string   "selection_prov2_action_3b", limit: 30
    t.string   "selection_prov3_action_3b", limit: 30
    t.string   "selection_prov4_action_3b", limit: 30
    t.string   "selection_prov5_action_3b", limit: 30
    t.string   "selection_prov1_action_3c", limit: 30
    t.string   "selection_prov2_action_3c", limit: 30
    t.string   "selection_prov3_action_3c", limit: 30
    t.string   "selection_prov4_action_3c", limit: 30
    t.string   "selection_prov5_action_3c", limit: 30
    t.integer  "selection_cost_action1_1a", limit: 4
    t.integer  "selection_cost_action2_1a", limit: 4
    t.integer  "selection_cost_action3_1a", limit: 4
    t.integer  "selection_cost_action4_1a", limit: 4
    t.integer  "selection_cost_action5_1a", limit: 4
    t.integer  "selection_cost_action1_1b", limit: 4
    t.integer  "selection_cost_action2_1b", limit: 4
    t.integer  "selection_cost_action3_1b", limit: 4
    t.integer  "selection_cost_action4_1b", limit: 4
    t.integer  "selection_cost_action5_1b", limit: 4
    t.integer  "selection_cost_action1_1c", limit: 4
    t.integer  "selection_cost_action2_1c", limit: 4
    t.integer  "selection_cost_action3_1c", limit: 4
    t.integer  "selection_cost_action4_1c", limit: 4
    t.integer  "selection_cost_action5_1c", limit: 4
    t.integer  "selection_cost_action1_2a", limit: 4
    t.integer  "selection_cost_action2_2a", limit: 4
    t.integer  "selection_cost_action3_2a", limit: 4
    t.integer  "selection_cost_action4_2a", limit: 4
    t.integer  "selection_cost_action5_2a", limit: 4
    t.integer  "selection_cost_action1_2b", limit: 4
    t.integer  "selection_cost_action2_2b", limit: 4
    t.integer  "selection_cost_action3_2b", limit: 4
    t.integer  "selection_cost_action4_2b", limit: 4
    t.integer  "selection_cost_action5_2b", limit: 4
    t.integer  "selection_cost_action1_2c", limit: 4
    t.integer  "selection_cost_action2_2c", limit: 4
    t.integer  "selection_cost_action3_2c", limit: 4
    t.integer  "selection_cost_action4_2c", limit: 4
    t.integer  "selection_cost_action5_2c", limit: 4
    t.integer  "selection_cost_action1_3a", limit: 4
    t.integer  "selection_cost_action2_3a", limit: 4
    t.integer  "selection_cost_action3_3a", limit: 4
    t.integer  "selection_cost_action4_3a", limit: 4
    t.integer  "selection_cost_action5_3a", limit: 4
    t.integer  "selection_cost_action1_3b", limit: 4
    t.integer  "selection_cost_action2_3b", limit: 4
    t.integer  "selection_cost_action3_3b", limit: 4
    t.integer  "selection_cost_action4_3b", limit: 4
    t.integer  "selection_cost_action5_3b", limit: 4
    t.integer  "selection_cost_action1_3c", limit: 4
    t.integer  "selection_cost_action2_3c", limit: 4
    t.integer  "selection_cost_action3_3c", limit: 4
    t.integer  "selection_cost_action4_3c", limit: 4
    t.integer  "selection_cost_action5_3c", limit: 4
    t.integer  "total_cost_action_1a",      limit: 4
    t.integer  "total_cost_action_1b",      limit: 4
    t.integer  "total_cost_action_1c",      limit: 4
    t.integer  "total_cost_action_2a",      limit: 4
    t.integer  "total_cost_action_2b",      limit: 4
    t.integer  "total_cost_action_2c",      limit: 4
    t.integer  "total_cost_action_3a",      limit: 4
    t.integer  "total_cost_action_3b",      limit: 4
    t.integer  "total_cost_action_3c",      limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "proposal_id",               limit: 4
    t.integer  "week_init_action_1a",       limit: 4
    t.integer  "week_init_action_1b",       limit: 4
    t.integer  "week_init_action_1c",       limit: 4
    t.integer  "week_init_action_2a",       limit: 4
    t.integer  "week_init_action_2b",       limit: 4
    t.integer  "week_init_action_2c",       limit: 4
    t.integer  "week_init_action_3a",       limit: 4
    t.integer  "week_init_action_3b",       limit: 4
    t.integer  "week_init_action_3c",       limit: 4
    t.integer  "week_fin_action_1a",        limit: 4
    t.integer  "week_fin_action_1b",        limit: 4
    t.integer  "week_fin_action_1c",        limit: 4
    t.integer  "week_fin_action_2a",        limit: 4
    t.integer  "week_fin_action_2b",        limit: 4
    t.integer  "week_fin_action_2c",        limit: 4
    t.integer  "week_fin_action_3a",        limit: 4
    t.integer  "week_fin_action_3b",        limit: 4
    t.integer  "week_fin_action_3c",        limit: 4
  end

  add_index "resources", ["proposal_id"], name: "index_resources_on_proposal_id", using: :btree

  create_table "sections", force: :cascade do |t|
    t.integer  "times_action_1a",     limit: 4
    t.integer  "times_action_1b",     limit: 4
    t.integer  "times_action_1c",     limit: 4
    t.integer  "times_action_2a",     limit: 4
    t.integer  "times_action_2b",     limit: 4
    t.integer  "times_action_2c",     limit: 4
    t.integer  "times_action_3a",     limit: 4
    t.integer  "times_action_3b",     limit: 4
    t.integer  "times_action_3c",     limit: 4
    t.string   "section1_fechaI_1a",  limit: 10
    t.string   "section1_fechaI_1b",  limit: 10
    t.string   "section1_fechaI_1c",  limit: 10
    t.string   "section1_fechaI_2a",  limit: 10
    t.string   "section1_fechaI_2b",  limit: 10
    t.string   "section1_fechaI_2c",  limit: 10
    t.string   "section1_fechaI_3a",  limit: 10
    t.string   "section1_fechaI_3b",  limit: 10
    t.string   "section1_fechaI_3c",  limit: 10
    t.string   "section2_fechaI_1a",  limit: 10
    t.string   "section2_fechaI_1b",  limit: 10
    t.string   "section2_fechaI_1c",  limit: 10
    t.string   "section2_fechaI_2a",  limit: 10
    t.string   "section2_fechaI_2b",  limit: 10
    t.string   "section2_fechaI_2c",  limit: 10
    t.string   "section2_fechaI_3a",  limit: 10
    t.string   "section2_fechaI_3b",  limit: 10
    t.string   "section2_fechaI_3c",  limit: 10
    t.string   "section3_fechaI_1a",  limit: 10
    t.string   "section3_fechaI_1b",  limit: 10
    t.string   "section3_fechaI_1c",  limit: 10
    t.string   "section3_fechaI_2a",  limit: 10
    t.string   "section3_fechaI_2b",  limit: 10
    t.string   "section3_fechaI_2c",  limit: 10
    t.string   "section3_fechaI_3a",  limit: 10
    t.string   "section3_fechaI_3b",  limit: 10
    t.string   "section3_fechaI_3c",  limit: 10
    t.string   "section4_fechaI_1a",  limit: 10
    t.string   "section4_fechaI_1b",  limit: 10
    t.string   "section4_fechaI_1c",  limit: 10
    t.string   "section4_fechaI_2a",  limit: 10
    t.string   "section4_fechaI_2b",  limit: 10
    t.string   "section4_fechaI_2c",  limit: 10
    t.string   "section4_fechaI_3a",  limit: 10
    t.string   "section4_fechaI_3b",  limit: 10
    t.string   "section4_fechaI_3c",  limit: 10
    t.string   "section5_fechaI_1a",  limit: 10
    t.string   "section5_fechaI_1b",  limit: 10
    t.string   "section5_fechaI_1c",  limit: 10
    t.string   "section5_fechaI_2a",  limit: 10
    t.string   "section5_fechaI_2b",  limit: 10
    t.string   "section5_fechaI_2c",  limit: 10
    t.string   "section5_fechaI_3a",  limit: 10
    t.string   "section5_fechaI_3b",  limit: 10
    t.string   "section5_fechaI_3c",  limit: 10
    t.string   "section6_fechaI_1a",  limit: 10
    t.string   "section6_fechaI_1b",  limit: 10
    t.string   "section6_fechaI_1c",  limit: 10
    t.string   "section6_fechaI_2a",  limit: 10
    t.string   "section6_fechaI_2b",  limit: 10
    t.string   "section6_fechaI_2c",  limit: 10
    t.string   "section6_fechaI_3a",  limit: 10
    t.string   "section6_fechaI_3b",  limit: 10
    t.string   "section6_fechaI_3c",  limit: 10
    t.string   "section7_fechaI_1a",  limit: 10
    t.string   "section7_fechaI_1b",  limit: 10
    t.string   "section7_fechaI_1c",  limit: 10
    t.string   "section7_fechaI_2a",  limit: 10
    t.string   "section7_fechaI_2b",  limit: 10
    t.string   "section7_fechaI_2c",  limit: 10
    t.string   "section7_fechaI_3a",  limit: 10
    t.string   "section7_fechaI_3b",  limit: 10
    t.string   "section7_fechaI_3c",  limit: 10
    t.string   "section8_fechaI_1a",  limit: 10
    t.string   "section8_fechaI_1b",  limit: 10
    t.string   "section8_fechaI_1c",  limit: 10
    t.string   "section8_fechaI_2a",  limit: 10
    t.string   "section8_fechaI_2b",  limit: 10
    t.string   "section8_fechaI_2c",  limit: 10
    t.string   "section8_fechaI_3a",  limit: 10
    t.string   "section8_fechaI_3b",  limit: 10
    t.string   "section8_fechaI_3c",  limit: 10
    t.string   "section9_fechaI_1a",  limit: 10
    t.string   "section9_fechaI_1b",  limit: 10
    t.string   "section9_fechaI_1c",  limit: 10
    t.string   "section9_fechaI_2a",  limit: 10
    t.string   "section9_fechaI_2b",  limit: 10
    t.string   "section9_fechaI_2c",  limit: 10
    t.string   "section9_fechaI_3a",  limit: 10
    t.string   "section9_fechaI_3b",  limit: 10
    t.string   "section9_fechaI_3c",  limit: 10
    t.string   "section10_fechaI_1a", limit: 10
    t.string   "section10_fechaI_1b", limit: 10
    t.string   "section10_fechaI_1c", limit: 10
    t.string   "section10_fechaI_2a", limit: 10
    t.string   "section10_fechaI_2b", limit: 10
    t.string   "section10_fechaI_2c", limit: 10
    t.string   "section10_fechaI_3a", limit: 10
    t.string   "section10_fechaI_3b", limit: 10
    t.string   "section10_fechaI_3c", limit: 10
    t.string   "section11_fechaI_1a", limit: 10
    t.string   "section11_fechaI_1b", limit: 10
    t.string   "section11_fechaI_1c", limit: 10
    t.string   "section11_fechaI_2a", limit: 10
    t.string   "section11_fechaI_2b", limit: 10
    t.string   "section11_fechaI_2c", limit: 10
    t.string   "section11_fechaI_3a", limit: 10
    t.string   "section11_fechaI_3b", limit: 10
    t.string   "section11_fechaI_3c", limit: 10
    t.string   "section12_fechaI_1a", limit: 10
    t.string   "section12_fechaI_1b", limit: 10
    t.string   "section12_fechaI_1c", limit: 10
    t.string   "section12_fechaI_2a", limit: 10
    t.string   "section12_fechaI_2b", limit: 10
    t.string   "section12_fechaI_2c", limit: 10
    t.string   "section12_fechaI_3a", limit: 10
    t.string   "section12_fechaI_3b", limit: 10
    t.string   "section12_fechaI_3c", limit: 10
    t.integer  "section1_dias_1a",    limit: 4
    t.integer  "section1_dias_1b",    limit: 4
    t.integer  "section1_dias_1c",    limit: 4
    t.integer  "section1_dias_2a",    limit: 4
    t.integer  "section1_dias_2b",    limit: 4
    t.integer  "section1_dias_2c",    limit: 4
    t.integer  "section1_dias_3a",    limit: 4
    t.integer  "section1_dias_3b",    limit: 4
    t.integer  "section1_dias_3c",    limit: 4
    t.integer  "section2_dias_1a",    limit: 4
    t.integer  "section2_dias_1b",    limit: 4
    t.integer  "section2_dias_1c",    limit: 4
    t.integer  "section2_dias_2a",    limit: 4
    t.integer  "section2_dias_2b",    limit: 4
    t.integer  "section2_dias_2c",    limit: 4
    t.integer  "section2_dias_3a",    limit: 4
    t.integer  "section2_dias_3b",    limit: 4
    t.integer  "section2_dias_3c",    limit: 4
    t.integer  "section3_dias_1a",    limit: 4
    t.integer  "section3_dias_1b",    limit: 4
    t.integer  "section3_dias_1c",    limit: 4
    t.integer  "section3_dias_2a",    limit: 4
    t.integer  "section3_dias_2b",    limit: 4
    t.integer  "section3_dias_2c",    limit: 4
    t.integer  "section3_dias_3a",    limit: 4
    t.integer  "section3_dias_3b",    limit: 4
    t.integer  "section3_dias_3c",    limit: 4
    t.integer  "section4_dias_1a",    limit: 4
    t.integer  "section4_dias_1b",    limit: 4
    t.integer  "section4_dias_1c",    limit: 4
    t.integer  "section4_dias_2a",    limit: 4
    t.integer  "section4_dias_2b",    limit: 4
    t.integer  "section4_dias_2c",    limit: 4
    t.integer  "section4_dias_3a",    limit: 4
    t.integer  "section4_dias_3b",    limit: 4
    t.integer  "section4_dias_3c",    limit: 4
    t.integer  "section5_dias_1a",    limit: 4
    t.integer  "section5_dias_1b",    limit: 4
    t.integer  "section5_dias_1c",    limit: 4
    t.integer  "section5_dias_2a",    limit: 4
    t.integer  "section5_dias_2b",    limit: 4
    t.integer  "section5_dias_2c",    limit: 4
    t.integer  "section5_dias_3a",    limit: 4
    t.integer  "section5_dias_3b",    limit: 4
    t.integer  "section5_dias_3c",    limit: 4
    t.integer  "section6_dias_1a",    limit: 4
    t.integer  "section6_dias_1b",    limit: 4
    t.integer  "section6_dias_1c",    limit: 4
    t.integer  "section6_dias_2a",    limit: 4
    t.integer  "section6_dias_2b",    limit: 4
    t.integer  "section6_dias_2c",    limit: 4
    t.integer  "section6_dias_3a",    limit: 4
    t.integer  "section6_dias_3b",    limit: 4
    t.integer  "section6_dias_3c",    limit: 4
    t.integer  "section7_dias_1a",    limit: 4
    t.integer  "section7_dias_1b",    limit: 4
    t.integer  "section7_dias_1c",    limit: 4
    t.integer  "section7_dias_2a",    limit: 4
    t.integer  "section7_dias_2b",    limit: 4
    t.integer  "section7_dias_2c",    limit: 4
    t.integer  "section7_dias_3a",    limit: 4
    t.integer  "section7_dias_3b",    limit: 4
    t.integer  "section7_dias_3c",    limit: 4
    t.integer  "section8_dias_1a",    limit: 4
    t.integer  "section8_dias_1b",    limit: 4
    t.integer  "section8_dias_1c",    limit: 4
    t.integer  "section8_dias_2a",    limit: 4
    t.integer  "section8_dias_2b",    limit: 4
    t.integer  "section8_dias_2c",    limit: 4
    t.integer  "section8_dias_3a",    limit: 4
    t.integer  "section8_dias_3b",    limit: 4
    t.integer  "section8_dias_3c",    limit: 4
    t.integer  "section9_dias_1a",    limit: 4
    t.integer  "section9_dias_1b",    limit: 4
    t.integer  "section9_dias_1c",    limit: 4
    t.integer  "section9_dias_2a",    limit: 4
    t.integer  "section9_dias_2b",    limit: 4
    t.integer  "section9_dias_2c",    limit: 4
    t.integer  "section9_dias_3a",    limit: 4
    t.integer  "section9_dias_3b",    limit: 4
    t.integer  "section9_dias_3c",    limit: 4
    t.integer  "section10_dias_1a",   limit: 4
    t.integer  "section10_dias_1b",   limit: 4
    t.integer  "section10_dias_1c",   limit: 4
    t.integer  "section10_dias_2a",   limit: 4
    t.integer  "section10_dias_2b",   limit: 4
    t.integer  "section10_dias_2c",   limit: 4
    t.integer  "section10_dias_3a",   limit: 4
    t.integer  "section10_dias_3b",   limit: 4
    t.integer  "section10_dias_3c",   limit: 4
    t.integer  "section11_dias_1a",   limit: 4
    t.integer  "section11_dias_1b",   limit: 4
    t.integer  "section11_dias_1c",   limit: 4
    t.integer  "section11_dias_2a",   limit: 4
    t.integer  "section11_dias_2b",   limit: 4
    t.integer  "section11_dias_2c",   limit: 4
    t.integer  "section11_dias_3a",   limit: 4
    t.integer  "section11_dias_3b",   limit: 4
    t.integer  "section11_dias_3c",   limit: 4
    t.integer  "section12_dias_1a",   limit: 4
    t.integer  "section12_dias_1b",   limit: 4
    t.integer  "section12_dias_1c",   limit: 4
    t.integer  "section12_dias_2a",   limit: 4
    t.integer  "section12_dias_2b",   limit: 4
    t.integer  "section12_dias_2c",   limit: 4
    t.integer  "section12_dias_3a",   limit: 4
    t.integer  "section12_dias_3b",   limit: 4
    t.integer  "section12_dias_3c",   limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "proposal_id",         limit: 4
  end

  add_index "sections", ["proposal_id"], name: "index_sections_on_proposal_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "ac_name",           limit: 255
    t.string   "email",             limit: 255
    t.string   "password_digest",   limit: 255
    t.string   "remember_digest",   limit: 255
    t.string   "address",           limit: 300
    t.string   "phone",             limit: 15
    t.string   "charge",            limit: 255
    t.integer  "user_type",         limit: 1,   default: 0, null: false
    t.string   "activation_digest", limit: 255
    t.integer  "activated",         limit: 1,   default: 0
    t.datetime "activated_at"
    t.string   "reset_digest",      limit: 255
    t.datetime "reset_sent_at"
    t.string   "api_key",           limit: 255
    t.string   "mission_vision",    limit: 500
    t.string   "yr_operation",      limit: 255
    t.string   "achievements",      limit: 500
    t.string   "programs_offered",  limit: 255
    t.integer  "beneficiarios",     limit: 4
    t.string   "comments",          limit: 500
    t.datetime "logo_updated_at"
    t.integer  "logo_file_size",    limit: 4
    t.string   "logo_content_type", limit: 255
    t.string   "logo_file_name",    limit: 255
    t.datetime "updated_at",                                null: false
    t.datetime "created_at",                                null: false
  end

  add_foreign_key "extras", "proyects"
  add_foreign_key "proyects", "users", name: "users_proyects_fk"
  add_foreign_key "recipients", "proyects", name: "proyects_recipients_fk"
  add_foreign_key "resources", "proposals"
end
