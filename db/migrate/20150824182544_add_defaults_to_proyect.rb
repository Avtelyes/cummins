class AddDefaultsToProyect < ActiveRecord::Migration
  def change
    change_column :proyects, :improvement_importance_a, :string, default: ""
    change_column :proyects, :improvement_importance_b, :string, default: ""
    change_column :proyects, :improvement_importance_c, :string, default: ""
    change_column :proyects, :improvement_importance_d, :string, default: ""
    change_column :proyects, :improvement_importance_e, :string, default: ""
    change_column :proyects, :improvement_importance_f, :string, default: ""
  end
end
