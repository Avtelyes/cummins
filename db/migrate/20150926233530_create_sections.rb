class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.integer :times_action_1a
      t.integer :times_action_1b
      t.integer :times_action_1c
      t.integer :times_action_2a
      t.integer :times_action_2b
      t.integer :times_action_2c
      t.integer :times_action_3a
      t.integer :times_action_3b
      t.integer :times_action_3c
      t.string :section1_fechaI_1a, :limit => 10
      t.string :section1_fechaI_1b, :limit => 10
      t.string :section1_fechaI_1c, :limit => 10
      t.string :section1_fechaI_2a, :limit => 10
      t.string :section1_fechaI_2b, :limit => 10
      t.string :section1_fechaI_2c, :limit => 10
      t.string :section1_fechaI_3a, :limit => 10
      t.string :section1_fechaI_3b, :limit => 10
      t.string :section1_fechaI_3c, :limit => 10
      t.string :section2_fechaI_1a, :limit => 10
      t.string :section2_fechaI_1b, :limit => 10
      t.string :section2_fechaI_1c, :limit => 10
      t.string :section2_fechaI_2a, :limit => 10
      t.string :section2_fechaI_2b, :limit => 10
      t.string :section2_fechaI_2c, :limit => 10
      t.string :section2_fechaI_3a, :limit => 10
      t.string :section2_fechaI_3b, :limit => 10
      t.string :section2_fechaI_3c, :limit => 10
      t.string :section3_fechaI_1a, :limit => 10
      t.string :section3_fechaI_1b, :limit => 10
      t.string :section3_fechaI_1c, :limit => 10
      t.string :section3_fechaI_2a, :limit => 10
      t.string :section3_fechaI_2b, :limit => 10
      t.string :section3_fechaI_2c, :limit => 10
      t.string :section3_fechaI_3a, :limit => 10
      t.string :section3_fechaI_3b, :limit => 10
      t.string :section3_fechaI_3c, :limit => 10
      t.string :section4_fechaI_1a, :limit => 10
      t.string :section4_fechaI_1b, :limit => 10
      t.string :section4_fechaI_1c, :limit => 10
      t.string :section4_fechaI_2a, :limit => 10
      t.string :section4_fechaI_2b, :limit => 10
      t.string :section4_fechaI_2c, :limit => 10
      t.string :section4_fechaI_3a, :limit => 10
      t.string :section4_fechaI_3b, :limit => 10
      t.string :section4_fechaI_3c, :limit => 10
      t.string :section5_fechaI_1a, :limit => 10
      t.string :section5_fechaI_1b, :limit => 10
      t.string :section5_fechaI_1c, :limit => 10
      t.string :section5_fechaI_2a, :limit => 10
      t.string :section5_fechaI_2b, :limit => 10
      t.string :section5_fechaI_2c, :limit => 10
      t.string :section5_fechaI_3a, :limit => 10
      t.string :section5_fechaI_3b, :limit => 10
      t.string :section5_fechaI_3c, :limit => 10
      t.string :section6_fechaI_1a, :limit => 10
      t.string :section6_fechaI_1b, :limit => 10
      t.string :section6_fechaI_1c, :limit => 10
      t.string :section6_fechaI_2a, :limit => 10
      t.string :section6_fechaI_2b, :limit => 10
      t.string :section6_fechaI_2c, :limit => 10
      t.string :section6_fechaI_3a, :limit => 10
      t.string :section6_fechaI_3b, :limit => 10
      t.string :section6_fechaI_3c, :limit => 10
      t.string :section7_fechaI_1a, :limit => 10
      t.string :section7_fechaI_1b, :limit => 10
      t.string :section7_fechaI_1c, :limit => 10
      t.string :section7_fechaI_2a, :limit => 10
      t.string :section7_fechaI_2b, :limit => 10
      t.string :section7_fechaI_2c, :limit => 10
      t.string :section7_fechaI_3a, :limit => 10
      t.string :section7_fechaI_3b, :limit => 10
      t.string :section7_fechaI_3c, :limit => 10
      t.string :section8_fechaI_1a, :limit => 10
      t.string :section8_fechaI_1b, :limit => 10
      t.string :section8_fechaI_1c, :limit => 10
      t.string :section8_fechaI_2a, :limit => 10
      t.string :section8_fechaI_2b, :limit => 10
      t.string :section8_fechaI_2c, :limit => 10
      t.string :section8_fechaI_3a, :limit => 10
      t.string :section8_fechaI_3b, :limit => 10
      t.string :section8_fechaI_3c, :limit => 10
      t.string :section9_fechaI_1a, :limit => 10
      t.string :section9_fechaI_1b, :limit => 10
      t.string :section9_fechaI_1c, :limit => 10
      t.string :section9_fechaI_2a, :limit => 10
      t.string :section9_fechaI_2b, :limit => 10
      t.string :section9_fechaI_2c, :limit => 10
      t.string :section9_fechaI_3a, :limit => 10
      t.string :section9_fechaI_3b, :limit => 10
      t.string :section9_fechaI_3c, :limit => 10
      t.string :section10_fechaI_1a, :limit => 10
      t.string :section10_fechaI_1b, :limit => 10
      t.string :section10_fechaI_1c, :limit => 10
      t.string :section10_fechaI_2a, :limit => 10
      t.string :section10_fechaI_2b, :limit => 10
      t.string :section10_fechaI_2c, :limit => 10
      t.string :section10_fechaI_3a, :limit => 10
      t.string :section10_fechaI_3b, :limit => 10
      t.string :section10_fechaI_3c, :limit => 10
      t.string :section11_fechaI_1a, :limit => 10
      t.string :section11_fechaI_1b, :limit => 10
      t.string :section11_fechaI_1c, :limit => 10
      t.string :section11_fechaI_2a, :limit => 10
      t.string :section11_fechaI_2b, :limit => 10
      t.string :section11_fechaI_2c, :limit => 10
      t.string :section11_fechaI_3a, :limit => 10
      t.string :section11_fechaI_3b, :limit => 10
      t.string :section11_fechaI_3c, :limit => 10
      t.string :section12_fechaI_1a, :limit => 10
      t.string :section12_fechaI_1b, :limit => 10
      t.string :section12_fechaI_1c, :limit => 10
      t.string :section12_fechaI_2a, :limit => 10
      t.string :section12_fechaI_2b, :limit => 10
      t.string :section12_fechaI_2c, :limit => 10
      t.string :section12_fechaI_3a, :limit => 10
      t.string :section12_fechaI_3b, :limit => 10
      t.string :section12_fechaI_3c, :limit => 10
      t.integer :section1_dias_1a
      t.integer :section1_dias_1b
      t.integer :section1_dias_1c
      t.integer :section1_dias_2a
      t.integer :section1_dias_2b
      t.integer :section1_dias_2c
      t.integer :section1_dias_3a
      t.integer :section1_dias_3b
      t.integer :section1_dias_3c
      t.integer :section2_dias_1a
      t.integer :section2_dias_1b
      t.integer :section2_dias_1c
      t.integer :section2_dias_2a
      t.integer :section2_dias_2b
      t.integer :section2_dias_2c
      t.integer :section2_dias_3a
      t.integer :section2_dias_3b
      t.integer :section2_dias_3c
      t.integer :section3_dias_1a
      t.integer :section3_dias_1b
      t.integer :section3_dias_1c
      t.integer :section3_dias_2a
      t.integer :section3_dias_2b
      t.integer :section3_dias_2c
      t.integer :section3_dias_3a
      t.integer :section3_dias_3b
      t.integer :section3_dias_3c
      t.integer :section4_dias_1a
      t.integer :section4_dias_1b
      t.integer :section4_dias_1c
      t.integer :section4_dias_2a
      t.integer :section4_dias_2b
      t.integer :section4_dias_2c
      t.integer :section4_dias_3a
      t.integer :section4_dias_3b
      t.integer :section4_dias_3c
      t.integer :section5_dias_1a
      t.integer :section5_dias_1b
      t.integer :section5_dias_1c
      t.integer :section5_dias_2a
      t.integer :section5_dias_2b
      t.integer :section5_dias_2c
      t.integer :section5_dias_3a
      t.integer :section5_dias_3b
      t.integer :section5_dias_3c
      t.integer :section6_dias_1a
      t.integer :section6_dias_1b
      t.integer :section6_dias_1c
      t.integer :section6_dias_2a
      t.integer :section6_dias_2b
      t.integer :section6_dias_2c
      t.integer :section6_dias_3a
      t.integer :section6_dias_3b
      t.integer :section6_dias_3c
      t.integer :section7_dias_1a
      t.integer :section7_dias_1b
      t.integer :section7_dias_1c
      t.integer :section7_dias_2a
      t.integer :section7_dias_2b
      t.integer :section7_dias_2c
      t.integer :section7_dias_3a
      t.integer :section7_dias_3b
      t.integer :section7_dias_3c
      t.integer :section8_dias_1a
      t.integer :section8_dias_1b
      t.integer :section8_dias_1c
      t.integer :section8_dias_2a
      t.integer :section8_dias_2b
      t.integer :section8_dias_2c
      t.integer :section8_dias_3a
      t.integer :section8_dias_3b
      t.integer :section8_dias_3c
      t.integer :section9_dias_1a
      t.integer :section9_dias_1b
      t.integer :section9_dias_1c
      t.integer :section9_dias_2a
      t.integer :section9_dias_2b
      t.integer :section9_dias_2c
      t.integer :section9_dias_3a
      t.integer :section9_dias_3b
      t.integer :section9_dias_3c
      t.integer :section10_dias_1a
      t.integer :section10_dias_1b
      t.integer :section10_dias_1c
      t.integer :section10_dias_2a
      t.integer :section10_dias_2b
      t.integer :section10_dias_2c
      t.integer :section10_dias_3a
      t.integer :section10_dias_3b
      t.integer :section10_dias_3c
      t.integer :section11_dias_1a
      t.integer :section11_dias_1b
      t.integer :section11_dias_1c
      t.integer :section11_dias_2a
      t.integer :section11_dias_2b
      t.integer :section11_dias_2c
      t.integer :section11_dias_3a
      t.integer :section11_dias_3b
      t.integer :section11_dias_3c
      t.integer :section12_dias_1a
      t.integer :section12_dias_1b
      t.integer :section12_dias_1c
      t.integer :section12_dias_2a
      t.integer :section12_dias_2b
      t.integer :section12_dias_2c
      t.integer :section12_dias_3a
      t.integer :section12_dias_3b
      t.integer :section12_dias_3c
      

      t.timestamps null: false
    end
  end
end
