class AddProposalRefToResources < ActiveRecord::Migration
  def change
    add_reference :resources, :proposal, index: true, foreign_key: true
  end
end
