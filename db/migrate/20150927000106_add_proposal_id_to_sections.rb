class AddProposalIdToSections < ActiveRecord::Migration
  def change
    add_reference :sections, :proposal, index: true
  end
end
