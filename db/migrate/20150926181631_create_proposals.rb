class CreateProposals < ActiveRecord::Migration
  def change
    create_table :proposals do |t|
      t.string :action_1a
      t.string :action_1b
      t.string :action_1c
      t.string :action_2a
      t.string :action_2b
      t.string :action_2c
      t.string :action_3a
      t.string :action_3b
      t.string :action_3c

      t.timestamps null: false
    end
  end
end
