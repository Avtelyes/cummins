class AddBeneficiariesNumberToProyects < ActiveRecord::Migration
  def change
    add_column :proyects, :benef_regular, :integer
    add_column :proyects, :benef_eventual, :integer
  end
end
