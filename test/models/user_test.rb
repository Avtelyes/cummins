# == Schema Information
#
# Table name: users
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  ac_name           :string(255)
#  email             :string(255)
#  password_digest   :string(255)
#  remember_digest   :string(255)
#  address           :string(300)
#  phone             :string(15)
#  charge            :string(255)
#  user_type         :integer          default(0), not null
#  activation_digest :string(255)
#  activated         :integer          default(0)
#  activated_at      :datetime
#  reset_digest      :string(255)
#  reset_sent_at     :datetime
#  api_key           :string(255)
#  mission_vision    :string(500)
#  yr_operation      :string(255)
#  achievements      :string(500)
#  programs_offered  :string(255)
#  beneficiarios     :integer
#  comments          :string(500)
#  logo_updated_at   :datetime
#  logo_file_size    :integer
#  logo_content_type :string(255)
#  logo_file_name    :string(255)
#  updated_at        :datetime         not null
#  created_at        :datetime         not null
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
