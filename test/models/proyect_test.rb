# == Schema Information
#
# Table name: proyects
#
#  id                          :integer          not null, primary key
#  user_id                     :integer          not null
#  logo_updated_at             :datetime
#  logo_content_type           :string(255)
#  logo_file_size              :integer
#  logo_file_name              :string(255)
#  topic                       :integer          default(0)
#  mission_vision              :string(500)
#  yr_operation                :string(255)
#  programs_offered            :string(255)
#  achievements                :string(500)
#  beneficiarios               :integer
#  comments                    :string(500)
#  step                        :integer          default(0)
#  existent_new                :integer          default(0)
#  idea_emerge                 :integer
#  idea_emerge_exp             :string(255)
#  procedure_taken             :integer
#  importance                  :string(255)
#  improvement_importance_a    :string(255)      default("")
#  improvement_importance_b    :string(255)      default("")
#  improvement_importance_c    :string(255)      default("")
#  improvement_importance_d    :string(255)      default("")
#  improvement_importance_e    :string(255)      default("")
#  improvement_importance_f    :string(255)      default("")
#  col_211_a                   :string(255)
#  col_211_b                   :string(255)
#  col_211_c                   :string(255)
#  col_211_d                   :string(255)
#  col_211_e                   :string(255)
#  col_212_a                   :string(255)
#  col_212_b                   :string(255)
#  col_212_c                   :string(255)
#  col_212_d                   :string(255)
#  col_212_e                   :string(255)
#  col_212_f                   :string(255)
#  col_oe1                     :string(255)
#  col_oe1_a                   :string(255)
#  col_oe1_b                   :string(255)
#  col_oe1_c                   :string(255)
#  col_oe1_d                   :string(255)
#  col_oe1_e                   :string(255)
#  col_oe2                     :string(255)
#  col_oe2_a                   :string(255)
#  col_oe2_b                   :string(255)
#  col_oe2_c                   :string(255)
#  col_oe2_d                   :string(255)
#  col_oe2_e                   :string(255)
#  col_oe3                     :string(255)
#  col_oe3_a                   :string(255)
#  col_oe3_b                   :string(255)
#  col_oe3_c                   :string(255)
#  col_oe3_d                   :string(255)
#  col_oe3_e                   :string(255)
#  ben_group                   :integer
#  ben_age_range               :integer          default(0)
#  ben_age_range2              :integer          default(0)
#  ben_risk                    :integer
#  ben_risk_exp                :string(255)
#  ben_number                  :integer          default(1)
#  ben_part_regular            :integer          default(0)
#  ben_part_eventual           :integer          default(0)
#  investment_a                :string(15)
#  investment_b                :string(15)
#  investment_c                :string(15)
#  investment_d                :string(15)
#  investment_e                :string(15)
#  investment_f                :string(255)
#  investment_sum              :string(15)
#  total_weeks                 :integer          default(0)
#  col_continuity_action1      :string(255)
#  col_continuity_indicator1   :string(255)
#  col_continuity_responsible1 :string(255)
#  col_continuity_action2      :string(255)
#  col_continuity_indicator2   :string(255)
#  col_continuity_responsible2 :string(255)
#  col_continuity_action3      :string(255)
#  col_continuity_indicator3   :string(255)
#  col_continuity_responsible3 :string(255)
#  updated_at                  :datetime         not null
#  created_at                  :datetime         not null
#  name                        :string(255)
#  importance_text             :string(255)      default("")
#  benef_regular               :integer
#  benef_eventual              :integer
#  importance_text_b           :string(255)      default("")
#  importance_text_c           :string(255)      default("")
#  importance_text_d           :string(255)      default("")
#  importance_text_e           :string(255)      default("")
#  importance_text_f           :string(255)      default("")
#  importance_text_g           :string(255)      default("")
#  importance_text_h           :string(255)      default("")
#  status                      :integer          default(0)
#

require 'test_helper'

class ProyectTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
