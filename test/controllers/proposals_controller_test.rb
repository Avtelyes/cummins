require 'test_helper'

class ProposalsControllerTest < ActionController::TestCase
  test "should get actions" do
    get :actions
    assert_response :success
  end

  test "should get activities" do
    get :activities
    assert_response :success
  end

  test "should get methodologies" do
    get :methodologies
    assert_response :success
  end

  test "should get impacts" do
    get :impacts
    assert_response :success
  end

  test "should get resources" do
    get :resources
    assert_response :success
  end

end
